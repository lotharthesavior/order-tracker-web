<?php

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

namespace Controllers;

/**
 * Class responsible for the application core navigation structure.
 * 
 * @author Savio <savio@minisisinc.com>
 */
class AppController extends \Marina\MasaController
{
	protected $container;

	/**
	 * Start the controller instantiating the Slim Container
	 * @todo move this to a controller parent class
	 */
	public function __construct($container){
		$this->container = $container;
	}

	/**
	 * Home Screen
	 * 
	 * @param Request $request
	 * @param Response $response
	 * @return Response $response
	 * @author Savio <savio@minisisinc.com>
	 */
	public function home(\Slim\Http\Request $request, \Slim\Http\Response $response){
		$this->observer(__METHOD__, $response);

		$breadcrumbs = [
			"Dashboard" => ["state" => "active"]
		];

		// var_dump($this->container->get('settings')['template-dir']);exit;

		$response = $this->container->view->render($response, "index.php", [
	    	'template_dir' => '/' . $this->container->get('settings')['template-dir'],
	    	'title'        => 'Welcome!',
	    	'page'         => "Order Tracker",
			'breadcrumbs'  => $breadcrumbs		
	    ]);

	   	return $response;
	}

        /**
         *
         */
        public function notAuthorized(\Slim\Http\Request $request, \Slim\Http\Response $response){
            $this->observer(__METHOD__, $response);

            $_SESSION['message'] = "Access denied!";
            $_SESSION['message-type'] = "danger";

            return $response->withStatus(302)->withHeader('Location', '/login');
        }

	/**
	 *
	 */
	public function example(\Slim\Http\Request $request, \Slim\Http\Response $response){
		$this->observer(__METHOD__, $response);
		
                $response = $this->container->view->render($response, "example.php", [
                        'template_dir' => '/'.$this->container->get('settings')['template-dir']
                ]); 

                return $response;
	}

	/**
	 * Handles file uploads
	 */
	public function uploadFile(\Slim\Http\Request $request, \Slim\Http\Response $response){
		$this->observer(__METHOD__);

		$upload_handler = new \Components\UploadHandler();
		$response = $upload_handler->get_response();

		$response = $this->placeNewFilesFinalDirectory($response);

		ob_clean();

		echo json_encode($response);

	}

	/**
	 * This method:
	 *     1. prepare a new name (hash)
	 *     2. move the upload to /uploads folder
	 * 
	 * @param Array $upload_response
	 */
	private function placeNewFilesFinalDirectory(Array $upload_response){

		$current_dir = str_replace("Controllers", "", __DIR__);
		$adapter = new \League\Flysystem\Adapter\Local($current_dir);
		$filesystem = new \League\Flysystem\Filesystem($adapter);

		foreach ($upload_response['files'] as $key =>  $file_object) {

            // building the new name
            $file_name = $file_object->name;
            $imageFileType = pathinfo(basename($file_name),PATHINFO_EXTENSION);
            $target_file = uniqid() . "." . $imageFileType;

            // moving the file to the new location
            $filesystem->copy('files/' . $file_name, 'uploads/' . $target_file );

            // changing the name in the object to return
            $original_name = $upload_response['files'][$key]->name;
            $upload_response['files'][$key]->name = $target_file;
            $upload_response['files'][$key]->url = str_replace(
                $original_name, 
                $target_file, 
                $upload_response['files'][$key]->url
            );
            $upload_response['files'][$key]->original_name = $file_name;
            unset($upload_response['files'][$key]->thumbnailUrl);
            unset($upload_response['files'][$key]->deleteUrl);
            unset($upload_response['files'][$key]->deleteType);
        }

        // removing the temporary directory
        $filesystem->deleteDir('files');

        return $upload_response;

	}

	/**
	 * 
	 */
	public function downloadFile(\Slim\Http\Request $request, \Slim\Http\Response $response, $args){
		$this->observer(__METHOD__);

		$database  = $request->getAttribute('database');
        $record    = $request->getAttribute('record');
        $file_name = $request->getAttribute('file');
        $extension = $request->getAttribute('extension');

        $this->container->{$database}->getFileInDatabase( $record, $file_name, $extension );

	}

}
