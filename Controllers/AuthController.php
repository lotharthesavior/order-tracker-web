<?php

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

namespace Controllers;

/**
 * Class responsible for the application core navigation structure.
 *
 * @author Savio <savio@savioresende.com.br>
 */
class AuthController extends \Marina\MasaController
{
    protected $container;

    /**
     * Start the controller instantiating the Slim Container
     * @todo move this to a controller parent class
     */
    public function __construct($container)
    {
        $this->container = $container;

        parent::__construct();
    }

    /**
     * Login POST
     *
     * @param Request $request
     * @param Response $response
     * @return Response $response
     */
    public function loginPost(\Slim\Http\Request $request, \Slim\Http\Response $response)
    {
        $this->observer(__METHOD__);

        $authorized = false;

        $post_data = $request->getParsedBody();

        $password = md5($post_data['password']);
        unset($post_data['password']);

        // this result must be only one
        $result = $this->container->users->search($post_data);

        // retrieve the id
        $the_id = reset($result->results)->id;

        // check if the result is more than 1 - if it is, there is a problem
        if (count($result->results) > 1) {
            $response->withStatus(403)->withHeader('Location', '/');
        }

        // check if the result is an object, if it is, the result might not be a collection of results
        if (count($result->results) < 1) {
            $return_message = \Helpers\UtilityHelper::prepareReturnMessage([
                'message' => 'invalid_credentials'
            ]);

            $_SESSION['message'] = $return_message;

            $_SESSION['message-type'] = 'danger';

            return $response->withStatus(302)->withHeader('Location', '/login');
        }

        // get the first item of the collection (array)
        $result_user = reset($result->results);

        // get the user record data
        $user = $result_user->file_content;

        if ($password === $user->password) {
            $authorized = true;
        }

        if ($authorized) {
            $_SESSION['user_logon'] = [];
            $_SESSION['user_logon']['ID'] = $the_id;
            $_SESSION['user_logon']['EMAIL'] = $user->email;
            $_SESSION['user_logon']['NAME'] = $user->name;
            $_SESSION['user_logon']['USER_TYPE'] = $user->user_type;
            $this->_createPersistentCookie($_SESSION['user_logon']);
        }

        return $response->withStatus(302)->withHeader('Location', '/');
    }

    /**
     * Generate Cookie to persist user connection
     * 
     * @param array $userData 
     */
    private function _createPersistentCookie (array $userData)
    {
        setcookie("resendes-house-persistent", bin2hex(json_encode($userData)), time() + (60 * 60 * 24));
    }

    /**
     * Logout
     *
     * @param Request $request
     * @param Response $response
     * @return Response $response
     * @author Savio <savio@savioresende.com.br>
     */
    public function logout(\Slim\Http\Request $request, \Slim\Http\Response $response)
    {
        $this->observer(__METHOD__);

        unset($_SESSION['user_logon']);

        setcookie("resendes-house-persistent", "", time()-3600);

        return $response->withStatus(302)->withHeader('Location', '/login');
    }

    /**
     * Login Form
     *
     * @param Request $request
     * @param Response $response
     * @return Response $response
     * @author Savio <savio@savioresende.com.br>
     */
    public function loginForm(\Slim\Http\Request $request, \Slim\Http\Response $response)
    {
        global $permissions_list;

        $this->observer(__METHOD__);

        $isAuthorized = \Helpers\AppHelper::loginValidate();
        if ($isAuthorized) {
            return $response->withStatus(302)->withHeader('Location', '/');
        }

        $response = $this->container->view->render($response, "login.php", [
            'template_dir' => '/' . $this->container->get('settings')['template-dir']
        ]);

    }

    /**
     * Register Form
     *
     * @param Request $request
     * @param Response $response
     * @return Response $response
     * @author Savio <savio@savioresende.com.br>
     */
    public function registerForm(\Slim\Http\Request $request, \Slim\Http\Response $response)
    {
        $this->observer(__METHOD__);

        $response = $this->container->view->render($response, "register.php", [
            'template_dir' => '/' . $this->container->get('settings')['template-dir']
        ]);

    }

    /**
     * Register Post
     *
     * @param Request $request
     * @param Response $response
     * @return Response $response | SUCCESS: /login OR ERROR: /register
     * @author Savio <savio@savioresende.com.br>
     */
    public function registerPost(\Slim\Http\Request $request, \Slim\Http\Response $response)
    {
        $this->observer(__METHOD__);

        $post_data = $request->getParsedBody();

        $this->logger->info('AuthController - method: registerPost - post_data: ' . json_encode($post_data));
        // echo "<pre>";var_dump($post_data);exit;

        $result = $this->container->users->register($post_data);
        // echo "<pre>";var_dump($_SESSION);exit;
        // echo "<pre>";var_dump($result);exit;

        if (isset($result['message'])) {
            $this->logger->info('AuthController - method: registerPost - result: ' . $result['message']);
        } else {
            $this->logger->info('AuthController - method: registerPost - result: success');
        }

        $return_message = \Helpers\UtilityHelper::prepareReturnMessage($result);
        // echo "<pre>";var_dump($return_message);exit;

        $_SESSION['message'] = $return_message;

        if (!$result_parsed['status']) {

            $_SESSION['message-type'] = 'danger';

            return $response->withStatus(302)->withHeader('Location', '/register');
        }

        $_SESSION['message-type'] = 'success';

        return $response->withStatus(302)->withHeader('Location', '/login');

    }

    /**
     * Forget Password Form
     *
     * @param Request $request
     * @param Response $response
     * @return Response $response
     * @author Savio <savio@savioresende.com.br>
     */
    public function forgetPasswordForm(\Slim\Http\Request $request, \Slim\Http\Response $response)
    {
        $this->observer(__METHOD__);

        $response = $this->container->view->render($response, "forget-password.php", [
            'template_dir' => '/' . $this->container->get('settings')['template-dir']
        ]);

    }

    /**
     * Forget Password Post
     *
     * @param Request $request
     * @param Response $response
     * @return Response $response
     * @author Savio <savio@savioresende.com.br>
     */
    public function forgetPasswordPost(\Slim\Http\Request $request, \Slim\Http\Response $response)
    {
        $this->observer(__METHOD__);

        exit("TODO: forget password post");
        $response = $this->container->view->render($response, "register.php", [
            'template_dir' => '/' . $this->container->get('settings')['template-dir']
        ]);

    }

    /**
     * New Password Form
     *
     * @param Request $request
     * @param Response $response
     * @return Response $response
     * @author Savio <savio@savioresende.com.br>
     */
    public function newPasswordForm(\Slim\Http\Request $request, \Slim\Http\Response $response)
    {
        $this->observer(__METHOD__);

        $response = $this->container->view->render($response, "new-password.php", [
            'template_dir' => '/' . $this->container->get('settings')['template-dir']
        ]);

    }

    /**
     * New Password Post
     *
     * @param Request $request
     * @param Response $response
     * @return Response $response
     * @author Savio <savio@savioresende.com.br>
     */
    public function newPasswordPost(\Slim\Http\Request $request, \Slim\Http\Response $response)
    {
        $this->observer(__METHOD__);

        exit("TODO: new password post");
        $response = $this->container->view->render($response, "register.php", [
            'template_dir' => '/' . $this->container->get('settings')['template-dir']
        ]);

    }
}
