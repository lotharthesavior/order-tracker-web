<?php

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

namespace Controllers\Generated;

/**
 * @todo finish this class after create parent controller
 */
class NotesController extends \Marina\MasaController
{

	protected $container;

	/**
	 * Start the controller instantiating the Slim Container
	 * @todo move this to a controller parent class
	 */
	public function __construct($container){
		$this->container = $container;
	}

	/**
 * Call after every method that render template
	 * 
	 * @param String $method (result of php magic constante __METHOD__)
	 */
	private function observerAfterTemplate( $method ){
		if( isset($_SESSION['message']) ){
			unset($_SESSION['message']);
		}

		if( isset($_SESSION['message-positive']) ){
			unset($_SESSION['message-positive']);
		}
	}

	/**
	 * Support Screen
	 * 
	 * @param Request $request
	 * @param Response $response
     *
	 * @return Response $response
	 */
	public function home(\Slim\Http\Request $request, \Slim\Http\Response $response){
	    $this->observer(__METHOD__);

	    $search = $request->getParam("search");

	    $search_statement = [];

	    $breadcrumbs = [
	        "Dashboard" => [
	            "url" => "/", 
	            "state" => ""
	        ],
			"Notes" => [
                "state" => "active"
            ]
	    ];

	    if( empty($search) ){
            $notes = $this->container->notes->findAll();
	    } else {
	    	$notes = $this->container->notes->search([
	    		"search" => $search
	    	]);
	    }
	    
	    $response = $this->container->view->render($response, "notes.php", [
	        'template_dir' => '/'.$this->container->get('settings')['template-dir'],
	        'notes'      => $notes,
	        'breadcrumbs'  => $breadcrumbs
	    ]);

	    $this->observerAfterTemplate(__METHOD__);

	    return $response;
	}

	/**
	 * Notes Search
	 * 
	 * @param Request $request
	 * @param Response $response
     *
	 * @return Response $response
	 */
	public function search(\Slim\Http\Request $request, \Slim\Http\Response $response){
		$this->observer(__METHOD__);

		$post_data = $request->getParsedBody();
		$original_post_data = $post_data;
	    unset($post_data['search']);
	    
	    $post_data = array_filter($post_data);;

	    exit('not implemented');

	    $response = $this->container->view->render($response, "notes.php", [
	        'template_dir'      => '/'.$this->container->get('settings')['template-dir'],
	        'post_data'         => $original_post_data
	    ]);

	    $this->observerAfterTemplate(__METHOD__);

	    return $response;
	}

	/**
	 * Central place for Search notes
	 * 
	 * @param array $filter
     *
	 * @return Doctrine\Common\Collections\ArrayCollection $notes
	 */
	private function searchNotes(array $filters){
		$this->observer(__METHOD__);

		if( !\Helpers\AppHelper::isAdministrator() ){
	    	$filters['CLIENT'] = $_SESSION['note_logon']["CLIENT"];
	    }
	    
	    $result = $this->container->notes->search($filters);

	    $notes = new \Doctrine\Common\Collections\ArrayCollection($result);
	    
	    return $notes;
	}

	/**
	 * Notes Creation
	 * 
	 * @param Request $request
	 * @param Response $response
     *
	 * @return Response $response
	 */
	public function create(\Slim\Http\Request $request, \Slim\Http\Response $response){
	    $this->observer(__METHOD__);

        $breadcrumbs = [
            "Dashboard" => [
                "url" => "/",
                "state" => ""
            ],
            "Notes" => [
                "url" => "/notes",
                "state" => ""
            ],
            "Note Creation" => [
                "state" => "active"
            ]
        ];

	    $response = $this->container->view->render($response, "notes-edit.php", [
	        'template_dir' => '/'.$this->container->get('settings')['template-dir'],
                'breadcrumbs' => $breadcrumbs
	    ]);

	    $this->observerAfterTemplate(__METHOD__);

	    return $response;
	}

	/**
	 * Notes Edition
	 * 
	 * @param Request $request
	 * @param Response $response
     *
	 * @return Response $response
	 */
	public function view(\Slim\Http\Request $request, \Slim\Http\Response $response){
		$this->observer(__METHOD__);

		// get the current note
	    $note = $this->container->notes->find($request->getAttribute('id'));

	    if (count($note->results) < 1) {
			return $response->withStatus(302)->withHeader('Location', '/notes');
		}

        $note = reset($note->results);

        $breadcrumbs = [
            "Dashboard" => [
                "url" => "/",
                "state" => ""
            ],
            "Notes" => [
                "url" => "/notes",
                "state" => ""
            ],
            ("Note View") => [
                "state" => "active"
            ]
        ];

	    // render the template
	    $response = $this->container->view->render($response, "notes-view.php", [
	        'template_dir' => '/'.$this->container->get('settings')['template-dir'],
            'breadcrumbs'  => $breadcrumbs,
	        'note'         => $note
	    ]);

	    $this->observerAfterTemplate(__METHOD__);

	    return $response;
	}

	/**
	 * Notes Edition
	 * 
	 * @param Request $request
	 * @param Response $response
	 * @return Response $response
	 * @author Savio <savio@savisavioresende.com.brr>
	 */
	public function edit (\Slim\Http\Request $request, \Slim\Http\Response $response)
	{
		$this->observer(__METHOD__);

        // get the current note
        $note = $this->container->notes->search( ['id' => $request->getAttribute('id')] );

        if (count($note->results) < 1) {
            return $response->withStatus(302)->withHeader('Location', '/notes');
        }

		$note = reset($note->results);

        $breadcrumbs = [
            "Dashboard" => [
                "url" => "/",
                "state" => ""
            ],
            "Notes" => [
                "url" => "/notes",
                "state" => ""
            ],
            "Notes Edition" => [
                "state" => "active"
            ]
        ];

	    // render template
	    $response = $this->container->view->render($response, "notes-edit.php", [
	        'template_dir'      => '/'.$this->container->get('settings')['template-dir'],
	        'edition'			=> 1,
	        'note'            => $note,
            'breadcrumbs'       => $breadcrumbs
	    ]);

	    $this->observerAfterTemplate(__METHOD__);

	    return $response;
	}

	/**
	 * Notes Edition POST
	 * 
	 * @param Request $request
	 * @param Response $response
     *
	 * @return Response $response
	 */
	public function editPost(\Slim\Http\Request $request, \Slim\Http\Response $response){
		$this->observer(__METHOD__);

		// get the current date
		$current_date = new \DateTime("now");

		// get post data
		$post_data = $request->getParsedBody();
	    
	   	$post_data = array_filter( $post_data );

	   	$post_data = $this->clearFilesParam( $post_data );

        // \Marina\MasaModelResponse
	    $result = $this->container->notes->save( $post_data );

        if( !$result->_result ){
//	    	$_SESSION['message'] = \Helpers\MessageHelper::translateFieldNameOnMessage( $result->_error );
	    	header('Location: ' . $_SERVER['HTTP_REFERER']);
	    	exit;
	    }

	    // $return = $this->container->notes->moveFilesToDatabase( $result->note );
	    // var_dump($return);exit;
	    
	    // $result->_message
	    $_SESSION['message'] = "Successfully updated the Note.";
	    $_SESSION['message-positive'] = 1;

	    return $response->withStatus(302)->withHeader('Location', '/notes');
	}

	/**
   	 * Convert the given array of files to:
   	 *     ([
   	 *         ... ,
   	 *         'files' => [
   	 *             [
   	 *                 'file_name' => string, 
   	 *                 'file_original_name' => string
   	 *             ]
   	 *         ]
   	 *     ])
   	 *
	 * @param array $post_data
	 * @return array $post_data
	 */
	private function clearFilesParam( array $post_data ){
		if(
	    	isset($post_data['files']) && !empty($post_data['files'])
	    	&& isset($post_data['file_name']) && !empty($post_data['file_name'])
	    	&& isset($post_data['file_original_name']) && !empty($post_data['file_original_name'])
	    ){
	    	$post_data['files'] = [];
	    	foreach ($post_data['file_name'] as $key => $file_name) {
	    		array_push($post_data['files'], [
		    		'file_name' => $file_name,
		    		'file_original_name' => $post_data['file_original_name'][$key]
		    	]);
	    	}
	    	unset($post_data['file_name']);
	    	unset($post_data['file_original_name']);
	    }

	    return $post_data;
	}

	/**
	 * Delete a Record
	 * 
	 * @param \Slim\Http\Request $request
	 * @param \Slim\Http\Response $response
     *
	 * @param array $args
	 */
	public function remove(\Slim\Http\Request $request, \Slim\Http\Response $response, array $args){
		$this->observer(__METHOD__);

		// \Marina\MasaModelResponse
		$result = $this->container->notes->deleteRecord($args['id']);
		// var_dump($result);exit;

		if( !$result->_result ){
	    	$_SESSION['message'] = \Helpers\MessageHelper::translateFieldNameOnMessage( $result->_error );
	    	header('Location: ' . $_SERVER['HTTP_REFERER']);
	    	exit;
	    }
	    
	    // $result->_message
	    $_SESSION['message'] = "Note successfully removed.";
	    $_SESSION['message-positive'] = 1;

		return $response->withStatus(302)->withHeader('Location', '/notes');

	}

}
