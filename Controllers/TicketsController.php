<?php

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

namespace Controllers;

/**
 * @todo finish this class after create parent controller
 */
class TicketsController extends \Marina\MasaController
{

	protected $container;

	/**
	 * Start the controller instantiating the Slim Container
	 * @todo move this to a controller parent class
	 */
	public function __construct($container){
		$this->container = $container;
	}

	/**
 * Call after every method that render template
	 * 
	 * @param String $method (result of php magic constante __METHOD__)
	 */
	private function observerAfterTemplate( $method ){
		if( isset($_SESSION['message']) ){
			unset($_SESSION['message']);
		}

		if( isset($_SESSION['message-positive']) ){
			unset($_SESSION['message-positive']);
		}
	}

	/**
	 * Support Screen
	 * 
	 * @param Request $request
	 * @param Response $response
	 * @return Response $response
	 * @author Savio <savio@savioresende.com.br>
	 */
	public function home(\Slim\Http\Request $request, \Slim\Http\Response $response){
	    $this->observer(__METHOD__);

	    $search = $request->getParam("search");

	    $search_statement = [];

	    $breadcrumbs = [
	        "Dashboard" => [
	            "url" => "/", 
	            "state" => ""
	        ],
			"Tickets" => [
                "state" => "active"
            ]
	    ];

	    if( empty($search) ){
            $tickets = $this->container->tickets->findAll();
	    } else {
	    	$tickets = $this->container->tickets->search([
	    		"search" => $search
	    	]);
	    }
	    
	    $response = $this->container->view->render($response, "tickets.php", [
	        'template_dir' => '/'.$this->container->get('settings')['template-dir'],
	        'tickets'      => $tickets,
	        'breadcrumbs'  => $breadcrumbs
	    ]);

	    $this->observerAfterTemplate(__METHOD__);

	    return $response;
	}

	/**
	 * Tickets Search
	 * 
	 * @param Request $request
	 * @param Response $response
	 * @return Response $response
	 */
	public function search(\Slim\Http\Request $request, \Slim\Http\Response $response){
		$this->observer(__METHOD__);

		$post_data = $request->getParsedBody();
		$original_post_data = $post_data;
	    unset($post_data['search']);
	    
	    $post_data = array_filter($post_data);;

	    exit('not implemented');

	    $response = $this->container->view->render($response, "tickets.php", [
	        'template_dir'      => '/'.$this->container->get('settings')['template-dir'],
	        'post_data'         => $original_post_data
	    ]);

	    $this->observerAfterTemplate(__METHOD__);

	    return $response;
	}

	/**
	 * Central place for Search tickets
	 * 
	 * @author Savio Resende <savio@savioresende.com.br>
	 * @param Array $filter
	 * @return Doctrine\Common\Collections\ArrayCollection $tickets
	 */
	private function searchTickets(Array $filters){
		$this->observer(__METHOD__);

		if( !\Helpers\AppHelper::isAdministrator() ){
	    	$filters['CLIENT'] = $_SESSION['user_logon']["CLIENT"];
	    }
	    
	    $result = $this->container->tickets->search($filters);

	    $tickets = new \Doctrine\Common\Collections\ArrayCollection($result);
	    
	    return $tickets;
	}

	/**
	 * Tickets Creation
	 * 
	 * @param Request $request
	 * @param Response $response
	 * @return Response $response
	 * @author Savio <savio@savioresende.com.br>
	 */
	public function create(\Slim\Http\Request $request, \Slim\Http\Response $response){
	    $this->observer(__METHOD__);

        $breadcrumbs = [
            "Dashboard" => [
                "url" => "/",
                "state" => ""
            ],
            "Tickets" => [
                "url" => "/tickets",
                "state" => ""
            ],
            "Tickets Creation" => [
                "state" => "active"
            ]
        ];

	    $response = $this->container->view->render($response, "tickets-edit.php", [
	        'template_dir' => '/'.$this->container->get('settings')['template-dir'],
                'breadcrumbs' => $breadcrumbs
	    ]);

	    $this->observerAfterTemplate(__METHOD__);

	    return $response;
	}

	/**
	 * Tickets Edition
	 * 
	 * @param Request $request
	 * @param Response $response
	 * @return Response $response
	 * @author Savio <savio@savioresende.com.br>
	 */
	public function view(\Slim\Http\Request $request, \Slim\Http\Response $response){
		$this->observer(__METHOD__);

		// get the current ticket
	    $ticket = $this->container->tickets->find([
	        'sisn' => $request->getAttribute('sisn')
	    ]);

	    $ticket = $this->container->tickets::cleanVersions($ticket);

	    $ticket = (object) $ticket;

	    $ticket->problem['_value'] = htmlspecialchars_decode($ticket->problem['_value']);

	    // validate permission
		if(
			!\Helpers\AppHelper::isAdministrator() 
        	&& \Helpers\AppHelper::isTicketOwner( $ticket )
        	&& \Helpers\AppHelper::isInTheWorkflow( $ticket )
        	&& \Helpers\AppHelper::isInTheWorkflow( $ticket )
        ){
			return $response->withStatus(302)->withHeader('Location', '/tickets');	
		}

	    // get the current ticket comments
	    $tickets_comment = [];

	    // get the current ticket tracket tests
	    $tickets_testers = [];

	    // get the current ticket tracker fixes
	    $tickets_fix = [];

	    // get relases
	    $releases = [];
	    
	    // get server
	    $servers = [];

	    // get client
	    $clients = [];

	    // get patrons
	    $patrons = [];

	    // render the template
	    $response = $this->container->view->render($response, "tickets-view.php", [
	        'template_dir'      => '/'.$this->container->get('settings')['template-dir'],
	        'ticket'            => $ticket,
	        'tickets_comment'   => $tickets_comment,
	        'tickets_testers'	=> $tickets_testers,
	        'tickets_fix'		=> $tickets_fix,
	        'patrons'			=> $patrons,
	        'releases'			=> $releases,
	        'servers'			=> $servers,
	        'clients'			=> $clients,
	        'PATRON_ID'         => $_SESSION['user_logon']["PATRON_ID"]
	    ]);

	    $this->observerAfterTemplate(__METHOD__);

	    return $response;
	}

	/**
	 * Tickets Edition
	 * 
	 * @param Request $request
	 * @param Response $response
	 * @return Response $response
	 * @author Savio <savio@savisavioresende.com.brr>
	 */
	public function edit(\Slim\Http\Request $request, \Slim\Http\Response $response){
		$this->observer(__METHOD__);

        // get the current ticket
		$ticket = $this->container->tickets->search( ['id' => $request->getAttribute('id')] );

		$ticket = $ticket->results[0];
        // var_dump($ticket);exit;

        $breadcrumbs = [
            "Dashboard" => [
                "url" => "/",
                "state" => ""
            ],
            "Tickets" => [
                "url" => "/tickets",
                "state" => ""
            ],
            "Tickets Edition" => [
                "state" => "active"
            ]
        ];

	    // render template
	    $response = $this->container->view->render($response, "tickets-edit.php", [
	        'template_dir'      => '/'.$this->container->get('settings')['template-dir'],
	        'edition'			=> 1,
	        'ticket'            => $ticket,
            'breadcrumbs'       => $breadcrumbs
	    ]);

	    $this->observerAfterTemplate(__METHOD__);

	    return $response;
	}

	/**
	 * Tickets Edition POST
	 * 
	 * @param Request $request
	 * @param Response $response
	 * @return Response $response
	 * @author Savio <savio@savioresende.com.br>
	 */
	public function editPost(\Slim\Http\Request $request, \Slim\Http\Response $response){
		$this->observer(__METHOD__);

		// get the current date
		$current_date = new \DateTime("now");

		// get post data
		$post_data = $request->getParsedBody();
	    
	   	$post_data = array_filter( $post_data );

	   	$post_data = $this->clearFilesParam( $post_data );

	   	// \Marina\MasaModelResponse
	    $result = $this->container->tickets->save( $post_data );

	    // var_dump($result->ticket);exit;
	    if( !$result->_result ){
	    	$_SESSION['message'] = \Helpers\MessageHelper::translateFieldNameOnMessage( $result->_error );
	    	header('Location: ' . $_SERVER['HTTP_REFERER']);
	    	exit;
	    }

	    $return = $this->container->tickets->moveFilesToDatabase( $result->ticket );
	    // var_dump($return);exit;
	    
	    // $result->_message
	    $_SESSION['message'] = "Successfully updated the Ticket.";
	    $_SESSION['message-positive'] = 1;

	    return $response->withStatus(302)->withHeader('Location', '/tickets');
	}

	/**
   	 * Convert the given array of files to:
   	 *     ([
   	 *         ... ,
   	 *         'files' => [
   	 *             [
   	 *                 'file_name' => string, 
   	 *                 'file_original_name' => string
   	 *             ]
   	 *         ]
   	 *     ])
   	 *
	 * @param Array $post_data
	 * @return Array $post_data
	 */
	private function clearFilesParam( Array $post_data ){
		if(
	    	isset($post_data['files']) && !empty($post_data['files'])
	    	&& isset($post_data['file_name']) && !empty($post_data['file_name'])
	    	&& isset($post_data['file_original_name']) && !empty($post_data['file_original_name'])
	    ){
	    	$post_data['files'] = [];
	    	foreach ($post_data['file_name'] as $key => $file_name) {
	    		array_push($post_data['files'], [
		    		'file_name' => $file_name,
		    		'file_original_name' => $post_data['file_original_name'][$key]
		    	]);
	    	}
	    	unset($post_data['file_name']);
	    	unset($post_data['file_original_name']);
	    }

	    return $post_data;
	}

	/**
	 * List comments of a ticket
	 * 
	 * @param Request $request
	 * @param Response $response
	 * @return Response $response
	 * @author Savio <savio@savioresende.com.br>
	 */
	public function comments(\Slim\Http\Request $request, \Slim\Http\Response $response){
		$tickets_comment = $this->container->tickets_comment->search([
	        'FK_TICKET_COM' => $request->getAttribute('ticket')
	    ]);
		
		return json_encode($tickets_comment);
	}

	/**
	 * Tickets Comment POST
	 * 
	 * @param Request $request
	 * @param Response $response
	 * @return Response $response
	 * @author Savio <savio@savioresende.com.br>
	 * @todo replace this date generation by the minisis default functionality
	 */
	public function commmentPost(\Slim\Http\Request $request, \Slim\Http\Response $response){
		$this->observer(__METHOD__);

		// return $response->withStatus(302)->withHeader('Location', '/tickets');
	}

	/**
	 * Delete a Record
	 * 
	 * @param \Slim\Http\Request $request
	 * @param \Slim\Http\Response $response
	 * @param Array $args
	 */
	public function remove(\Slim\Http\Request $request, \Slim\Http\Response $response, array $args){
		$this->observer(__METHOD__);

		// \Marina\MasaModelResponse
		$result = $this->container->tickets->deleteRecord($args['id']);
		// var_dump($result);exit;

		if( !$result->_result ){
	    	$_SESSION['message'] = \Helpers\MessageHelper::translateFieldNameOnMessage( $result->_error );
	    	header('Location: ' . $_SERVER['HTTP_REFERER']);
	    	exit;
	    }
	    
	    // $result->_message
	    $_SESSION['message'] = "Ticket successfully removed.";
	    $_SESSION['message-positive'] = 1;

		return $response->withStatus(302)->withHeader('Location', '/tickets');

	}

}
