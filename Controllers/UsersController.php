<?php

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

namespace Controllers;

/**
 * @todo finish this class after create parent controller
 */
class UsersController extends \Marina\MasaController
{

	protected $container;

	/**
	 * Start the controller instantiating the Slim Container
	 * @todo move this to a controller parent class
	 */
	public function __construct($container){
		$this->container = $container;
	}

	/**
 * Call after every method that render template
	 * 
	 * @param String $method (result of php magic constante __METHOD__)
	 */
	private function observerAfterTemplate( $method ){
		if( isset($_SESSION['message']) ){
			unset($_SESSION['message']);
		}

		if( isset($_SESSION['message-positive']) ){
			unset($_SESSION['message-positive']);
		}
	}

	/**
	 * Support Screen
	 * 
	 * @param Request $request
	 * @param Response $response
     *
	 * @return Response $response
	 */
	public function home(\Slim\Http\Request $request, \Slim\Http\Response $response){
	    $this->observer(__METHOD__);

	    $search = $request->getParam("search");

	    $search_statement = [];

	    $breadcrumbs = [
	        "Dashboard" => [
	            "url" => "/", 
	            "state" => ""
	        ],
			"Users" => [
                "state" => "active"
            ]
	    ];

	    if( empty($search) ){
            $users = $this->container->users->findAll();
	    } else {
	    	$users = $this->container->users->search([
	    		"search" => $search
	    	]);
	    }
	    
	    $response = $this->container->view->render($response, "users.php", [
	        'template_dir' => '/'.$this->container->get('settings')['template-dir'],
	        'users'      => $users,
	        'breadcrumbs'  => $breadcrumbs
	    ]);

	    $this->observerAfterTemplate(__METHOD__);

	    return $response;
	}

	/**
	 * Users Search
	 * 
	 * @param Request $request
	 * @param Response $response
	 * @return Response $response
	 */
	public function search(\Slim\Http\Request $request, \Slim\Http\Response $response){
		$this->observer(__METHOD__);

		$post_data = $request->getParsedBody();
		$original_post_data = $post_data;
	    unset($post_data['search']);
	    
	    $post_data = array_filter($post_data);

	    exit('not implemented');

	    $response = $this->container->view->render($response, "users.php", [
	        'template_dir'      => '/'.$this->container->get('settings')['template-dir'],
	        'post_data'         => $original_post_data
	    ]);

	    $this->observerAfterTemplate(__METHOD__);

	    return $response;
	}

	/**
	 * Central place for Search users
	 * 
	 * @author Savio Resende <savio@savioresende.com.br>
	 * @param array $filter
	 * @return Doctrine\Common\Collections\ArrayCollection $users
	 */
	private function searchUsers(array $filters){
		$this->observer(__METHOD__);

		if( !\Helpers\AppHelper::isAdministrator() ){
	    	$filters['CLIENT'] = $_SESSION['user_logon']["CLIENT"];
	    }
	    
	    $result = $this->container->users->search($filters);

	    $users = new \Doctrine\Common\Collections\ArrayCollection($result);
	    
	    return $users;
	}

	/**
	 * Users Creation
	 * 
	 * @param Request $request
	 * @param Response $response
	 * @return Response $response
	 * @author Savio <savio@savioresende.com.br>
	 */
	public function create(\Slim\Http\Request $request, \Slim\Http\Response $response){
	    $this->observer(__METHOD__);

        $breadcrumbs = [
            "Dashboard" => [
                "url" => "/",
                "state" => ""
            ],
            "Users" => [
                "url" => "/users",
                "state" => ""
            ],
            "User Creation" => [
                "state" => "active"
            ]
        ];

	    $response = $this->container->view->render($response, "users-edit.php", [
	        'template_dir' => '/'.$this->container->get('settings')['template-dir'],
                'breadcrumbs' => $breadcrumbs
	    ]);

	    $this->observerAfterTemplate(__METHOD__);

	    return $response;
	}

	/**
	 * Users Edition
	 * 
	 * @param Request $request
	 * @param Response $response
     *
	 * @return Response $response
	 */
	public function view(\Slim\Http\Request $request, \Slim\Http\Response $response){
		$this->observer(__METHOD__);

		// get the current user
	    $user = $this->container->users->find($request->getAttribute('id'));

	    if (count($user->results) < 1) {
			return $response->withStatus(302)->withHeader('Location', '/users');
		}

        $user = reset($user->results);

        $breadcrumbs = [
            "Dashboard" => [
                "url" => "/",
                "state" => ""
            ],
            "Users" => [
                "url" => "/users",
                "state" => ""
            ],
            ("User View") => [
                "state" => "active"
            ]
        ];

	    // render the template
	    $response = $this->container->view->render($response, "users-view.php", [
	        'template_dir' => '/'.$this->container->get('settings')['template-dir'],
            'breadcrumbs'  => $breadcrumbs,
	        'user'         => $user
	    ]);

	    $this->observerAfterTemplate(__METHOD__);

	    return $response;
	}

	/**
	 * Users Edition
	 * 
	 * @param Request $request
	 * @param Response $response
	 * @return Response $response
	 * @author Savio <savio@savisavioresende.com.brr>
	 */
	public function edit(\Slim\Http\Request $request, \Slim\Http\Response $response){
		$this->observer(__METHOD__);

        // get the current user
        $user = $this->container->users->search( ['id' => $request->getAttribute('id')] );

        if (count($user->results) < 1) {
            return $response->withStatus(302)->withHeader('Location', '/users');
        }

		$user = reset($user->results);

        $breadcrumbs = [
            "Dashboard" => [
                "url" => "/",
                "state" => ""
            ],
            "Users" => [
                "url" => "/users",
                "state" => ""
            ],
            "Users Edition" => [
                "state" => "active"
            ]
        ];

	    // render template
	    $response = $this->container->view->render($response, "users-edit.php", [
	        'template_dir'      => '/'.$this->container->get('settings')['template-dir'],
	        'edition'			=> 1,
	        'user'            => $user,
            'breadcrumbs'       => $breadcrumbs
	    ]);

	    $this->observerAfterTemplate(__METHOD__);

	    return $response;
	}

	/**
	 * Users Edition POST
	 * 
	 * @param Request $request
	 * @param Response $response
	 * @return Response $response
	 * @author Savio <savio@savioresende.com.br>
	 */
	public function editPost(\Slim\Http\Request $request, \Slim\Http\Response $response){
		$this->observer(__METHOD__);

		// get the current date
		$current_date = new \DateTime("now");

		// get post data
		$post_data = $request->getParsedBody();
	    
	   	$post_data = array_filter( $post_data );

	   	$post_data = $this->clearFilesParam( $post_data );

        // \Marina\MasaModelResponse
	    $result = $this->container->users->save( $post_data );

        if( !$result->_result ){
//	    	$_SESSION['message'] = \Helpers\MessageHelper::translateFieldNameOnMessage( $result->_error );
	    	header('Location: ' . $_SERVER['HTTP_REFERER']);
	    	exit;
	    }

	    // $return = $this->container->users->moveFilesToDatabase( $result->user );
	    // var_dump($return);exit;
	    
	    // $result->_message
	    $_SESSION['message'] = "Successfully updated the User.";
	    $_SESSION['message-positive'] = 1;

	    return $response->withStatus(302)->withHeader('Location', '/users');
	}

	/**
   	 * Convert the given array of files to:
   	 *     ([
   	 *         ... ,
   	 *         'files' => [
   	 *             [
   	 *                 'file_name' => string, 
   	 *                 'file_original_name' => string
   	 *             ]
   	 *         ]
   	 *     ])
   	 *
	 * @param array $post_data
	 * @return array $post_data
	 */
	private function clearFilesParam( array $post_data ){
		if(
	    	isset($post_data['files']) && !empty($post_data['files'])
	    	&& isset($post_data['file_name']) && !empty($post_data['file_name'])
	    	&& isset($post_data['file_original_name']) && !empty($post_data['file_original_name'])
	    ){
	    	$post_data['files'] = [];
	    	foreach ($post_data['file_name'] as $key => $file_name) {
	    		array_push($post_data['files'], [
		    		'file_name' => $file_name,
		    		'file_original_name' => $post_data['file_original_name'][$key]
		    	]);
	    	}
	    	unset($post_data['file_name']);
	    	unset($post_data['file_original_name']);
	    }

	    return $post_data;
	}

	/**
	 * Delete a Record
	 * 
	 * @param \Slim\Http\Request $request
	 * @param \Slim\Http\Response $response
	 * @param array $args
	 */
	public function remove(\Slim\Http\Request $request, \Slim\Http\Response $response, array $args){
		$this->observer(__METHOD__);

		// \Marina\MasaModelResponse
		$result = $this->container->users->deleteRecord($args['id']);
		// var_dump($result);exit;

		if( !$result->_result ){
	    	$_SESSION['message'] = \Helpers\MessageHelper::translateFieldNameOnMessage( $result->_error );
	    	header('Location: ' . $_SERVER['HTTP_REFERER']);
	    	exit;
	    }
	    
	    // $result->_message
	    $_SESSION['message'] = "User successfully removed.";
	    $_SESSION['message-positive'] = 1;

		return $response->withStatus(302)->withHeader('Location', '/users');

	}

}
