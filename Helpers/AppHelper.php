<?php

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

use \Models\Patron;

namespace Helpers;

/** 
 * Helper Class.
 * 
 * @author Savio <savio@minisisinc.com>
 */
class AppHelper
{

	/**
	 * Validate if there is a user logged in
	 * 
	 * @todo find a better way
	 * 
	 * @return bool
	 */
	public static function loginValidate()
	{
		return isset($_SESSION['user_logon']) && !empty($_SESSION['user_logon']);
	}

	/**
	 * Verify if current logged user is administrator
	 * 
	 * @return bool
	 */
	public static function isAdministrator(){
		if(!AppHelper::loginValidate())
			return false;
		
		return $_SESSION['user_logon']['PATRON_TYPE'] == 'Administrator';
	}

	/**
	 * Verify if the current User is the owner of the ticket
	 * 
	 * @param stdClass $ticket
	 * @return bool
	 */
	public static function isTicketOwner( \stdClass $ticket ){
		global $model_config;

		return $ticket->reported_by[0] == $_SESSION['user_logon']['PATRON_ID'];
	}

	/**
	 * Verify if the current logged user is in the workflow
	 * 
	 * @param stdClass $ticket
	 * @return bool
	 */
    public static function isInTheWorkflow( \stdClass $ticket ){
    	global $model_config;

    	// if resposible_grp is empty, return false
    	if( empty($ticket->responsible_grp[0]) ){
    		return false;
    	}

    	// remove all other responsibles
    	$responsibles_left = array_filter( $ticket->responsible_grp, function( $responsible_grp ){
    		return $responsible_grp['fk_responsible'] == $_SESSION['user_logon']['SISN'];
    	});

    	return count($responsibles_left) > 0;
    }

    /**
     * Verify if the user logged in is from the same organization of ticket
     * 
     * @todo
     * @param $stdClass ticket
     * @return bool
     */
   	public function isFromTheSameOrganization( \stdClass $ticket){

   		// echo "<pre>";var_dump($ticket);exit;

   	}

}