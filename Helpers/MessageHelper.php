<?php

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

namespace Helpers;

/** 
 * Helper Class.
 * 
 * @author Savio <savio@minisisinc.com>
 */
class MessageHelper
{

	/**
     * Method to translate the message to the business
     * 
     * @todo find a better way to do this
     * @param String $message 
     * @return String $message
     */
    public static function translateFieldNameOnMessage( $message ){
        $message = str_replace("problem", "Brief Description", $message);
        
        return $message;
    }

}
