<?php

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

namespace Helpers;

/** 
 * Helper Class.
 * 
 * @author Savio <savio@minisisinc.com>
 */
class UtilityHelper
{

	/**
	 * Import data from JSON to MINISIS Database
	 *
	 * Example of usage:
	 * 	$utility = new \Helpers\UtilityHelper();
	 * 	$model = new \Models\Documentation(['username'=>'MT','userpassword'=>'MT']);
	 *	$utility->importDataFromJson( "data_insert_documentation.json", $model );
	 *
	 * @param
	 * @param
	 * @param
	 * @return void
	 */
	public function importDataFromJson( $json_file, $database_model ){

		$raw_data = file_get_contents($json_file);
		$data_to_insert = json_decode($raw_data, true);

		foreach ($data_to_insert as $key => $value) {
			$database_model->save($value);
		}
		
	}

        /**
         * Prepare the message to be returned to the user
         *
         * @param array $process_return
         * [
         *     'message' => string
         * ]
         *
         * @return string
         */
        public static function prepareReturnMessage (array $process_return)
        {
            $messages_json = file_get_contents(__DIR__ . "/../messages.json");
            $messages = json_decode($messages_json, true);

            switch( $process_return['message'] ){
                case "registration_missing_fields":
                    $return_message = $messages[$process_return['message']] 
                                      . implode(", ", $process_return['message-detail']['missing_fields']) 
                                      . ".";
                    break;
                case "invalid_data_format":
                    $return_message = $messages[$process_return['message']]
                                      . implode(", ", $process_return['message-detail']['wrong_type_fields'])
                                      . ".";
                    break;
                case "error_constraint":
                    $return_message = $process_return['message-detail'];
                    break;
                case "invalid_credentials":
                case "register_error":
                case "register_registered_email":
                default:
                    $return_message = $messages[$process_return['message']];
                    break;
            }

            return $return_message;
        }

}
