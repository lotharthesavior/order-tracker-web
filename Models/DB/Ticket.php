<?php

/**
 * Users Model
 *
 * This is a Model to keep the business rules.
 *
 * @author Savio Resende <savio@minisisinc.com>
 */

namespace Models\DB;

use \Marina\Marina;
use \Marina\MasaDAL;
use \Marina\MasaModel;

class Ticket implements MasaModel {

    protected $_id;
    protected $_title;
    protected $_description;
    protected $_status;
    protected $_type;
    protected $_files;

    public function __set($name, $value){
        $this->{$name} = $value;
    }

    public function __get($name){
        return $this->{$name};
    }

    public function __construct(\stdClass $ticket_data){

        if( isset($ticket_data->id) ){
            $this->_id = $ticket_data->id;
        }

        if( isset($ticket_data->title) ){
            $this->_title = $ticket_data->title;
        }

        if( isset($ticket_data->description) ){
            $this->_description = $ticket_data->description;
        }

        if( isset($ticket_data->status) ){
            $this->_status = $ticket_data->status;
        }

        if( isset($ticket_data->type) ){
            $this->_type = $ticket_data->type;
        }

        if( isset($ticket_data->files) ){
            $this->_files = $ticket_data->files;
        }

    }

}
