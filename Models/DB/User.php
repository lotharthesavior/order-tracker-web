<?php

/**
 * Users Model
 *
 * This is a Model to keep the business rules.
 *
 * @author Savio Resende <savio@minisisinc.com>
 */

namespace Models\DB;

class User {

    protected $_name;
    protected $_emai;
    protected $_password;
    protected $_registered_at;
    protected $_user_type;

    public function __set($name, $value){
        $this->{$name} = $value;
    }

    public function __get($name){
        return $this->{$name};
    }

    public static function getUserInstance(\stdClass $user_data){
        $user = new User();
        
        if( isset($user_data->name) ){
            $user->_name = $user_data->name;
        }

        if( isset($user_data->email) ){
            $user->_email = $user_data->email;
        }

        if( isset($user_data->password) ){
            $user->_password = $user_data->password;
        }

        if( isset($user_data->registered_at) ){
            $user->_registered_at = $user_data->registered_at;
        }

        if( isset($user_data->user_type) ){
            $user->_user_type = $user_data->user_type;
        }

        return $user;
    }

}
