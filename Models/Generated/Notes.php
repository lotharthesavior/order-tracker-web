<?php

/**
 * Notes Model
 *
 * This is a Model to keep the business rules.
 *
 * @author Savio Resende <savio@minisisinc.com>
 */

namespace Models\Generated;

use \Marina\Marina;

class Notes extends Marina {

	protected $database = "notes";

    protected $fields = [
        "title",
"content",

    ];

	protected $required_fields = [
        
	];

    protected $constraints = [ 
        
    ];

	/**
	 * Constructor
     *
	 * @return $this instance
	 */
	public function __construct() {
	    $config_param['database'] = $this->database;

	    parent::__construct($config_param);
	}

    /**
     * Fill the Single Model
     * 
     * @param array $data
     * @return Note
     */
    protected function fillModel(array $data){
        $note_instance = new Note;

        foreach($data as $key => $value){
            $note_instance->{$key} = $value;
        }

        return $note_instance;
    }

	/**
	 * Find Page
	 *
	 * @param int $sisn
	 */
	public function find($id) {
        $result = $this->search(["id" => $id]);

        return $result;
	}

	/**
	 * 
	 */
	public function findAll(){
		$result = $this->search([]);
		
		return $result;
	}

    /**
     * @internal {search clusters section} is the part where we can search multiple fields using only one expression
     *
     * @param array $params
     */
    public function search(array $params, $recall = false)
    {
        // search clusters section
        if( isset($params["search"]) && !empty($params["search"]) ){
            foreach ($this->fields as $field) {
                $params[$field] = $params['search'];
            }
            $params['logic'] = 1;
            unset($params["search"]);
        }

        $results = parent::searchRecords($params, $recall);

        $results->results = $this->_filterSchema($results->results);

        return $results;
    }

    /**
     *
     */
    public function save(array $client_data)
    {
        return parent::save($client_data);
    }

}
