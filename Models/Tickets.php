<?php

/**
 * Tracker Model
 *
 * This is a Model to keep the business rules.
 *
 * @author Savio Resende <savio@minisisinc.com>
 */

namespace Models;

use \Marina\Marina;
use \Marina\MasaDAL;
use \Marina\MasaModel;
use \Marina\MasaModelResponse;

class Tickets extends Marina implements MasaDAL {

	protected $is_bag = true;

	protected $database = "tickets";

	// todo: move the responsibility to keep it in the Model
    protected $fields = [
        "title",
        "description",
        "status",
        "type"
    ];

    /**
     * This attribute  uses the CONSTANTS from php filters
     * Reference: http://php.net/manual/en/filter.filters.validate.php
     * @todo: check if this is implemented!
     */
    protected $field_type = [
        // "email" => FILTER_VALIDATE_EMAIL
    ];

    protected $required_fields = [
        "title",
        "description"
	];

    protected $constraints = [ 
        // "email" => "unique"
    ];

	// TODO: implement construct and put the proper configurations from this Mode in it

	/**
	 * Constructor
	 *
	 * @param Array (
	 * 	'applicationid' => string
	 * 	'database' => string
	 * 	'report' => string
	 * 	'command' => string
	 * 	'username' => string
	 * 	'userpassword' => string
	 * )
	 * @return $this instance
	 * @internal config file with json $config
	 */
	public function __construct() {
	    $config_param['database'] = $this->database;

	    parent::__construct($config_param);
	}

	/**
	 * Find Page
	 *
	 * @param int $sisn
	 */
	public function find($id) {
		$result = $this->search(["id" => $id]);

		return $result;
	}

	/**
	 * 
	 */
	public function findAll(){
		$result = $this->search([]);

		return $result;
	}

	/**
	 * 
	 */
	public function search (array $params, $recall = false)
    {
		// search clusters
		if( isset($params["search"]) && !empty($params["search"]) ){
			$params['title'] = $params["search"];
			$params['description'] = $params["search"];
			$params['logic'] = 1;
			unset($params["search"]);
		}

		$results = parent::searchRecords($params, $recall);

        $results->results = $this->_filterSchema($results->results);

		// return parent::search($params, $recall);
        return $results;
	}

	/**
	 * Save current Record
	 *
	 * @param String serialized data
	 * @return \Marina\MasaModelResponse
	 */
	public function save(Array $client_data) {
		$result = $this->validate($client_data);

		$client_data = $this->clearEmptyFilesArray($client_data);
		
		$result = parent::save($client_data);

		if( $result->_result ){
			$client_data['id'] = $result->_message;
			$result->ticket = new \Models\DB\Ticket((object) $client_data);
		}

		return $result;
	}

	/**
	 * 
	 */
	private function clearEmptyFilesArray($client_data){
		if( 
			isset($client_data['files'])
			&& empty(array_filter($client_data['files']))
		)
			unset($client_data['files']);

		return $client_data;
	}

	/**
	 * Validate required fields
	 *
	 * @param Array $client_data
	 * @return Array with result (bool) and message (string)
	 */
	public function validate(Array $client_data){
		$missing_fields = [];

		// echo "<pre>";var_dump($client_data);exit;
		$_SESSION['form_data'] = $client_data;

		// validate required fields
		if( isset($this->required_fields) ){
			$missing_fields = $this->getMissingFields($client_data);
			// echo "<pre>";var_dump($missing_fields);exit;

			if( !empty($missing_fields) ){
				$missing_string = implode(", ", $missing_fields);
				return [
					"result" => false,
					"message" => "Missing fields: " . ($missing_string) . "."
				];
			}

			// echo "<pre>";var_dump($_SESSION['form_data']);exit;
			unset($_SESSION['form_data']);
			return [ "result" => true ];
		}

		return [
			"result" => true,
			"message" => "No required fields specified."
		];

	}

	/**
	 * Get the missiung fields of a client data
	 * 
	 * @param Array $client_data
	 * @return Array $missing_fields
	 */
	private function getMissingFields($client_data){
		$missing_fields = [];
		// echo "<pre>";var_dump($client_data);exit;

		if( empty($this->required_fields) ){
			return $missing_fields;
		}

		foreach ($this->required_fields as $key => $value) {
			if( 
				!isset($client_data[$value]) 
				|| (
					isset($client_data[$value])
					&& empty($client_data[$value])
				)
			){
				array_push($missing_fields, $value);
			}
		}

		return $missing_fields;
	}

}

