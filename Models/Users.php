<?php

/**
 * Users Model
 *
 * This is a Model to keep the business rules.
 *
 * @author Savio Resende <savio@minisisinc.com>
 */

namespace Models;

use \Marina\Marina;

class Users extends Marina {

	protected $database = "users";

    protected $fields = [
        "name",
        "email",
        "password",
        "user_type"
    ];

    /**
     * This attribute  uses the CONSTANTS from php filters
     * Reference: http://php.net/manual/en/filter.filters.validate.php
     * @todo: check if this is implemented!
     */
    protected $field_type = [
        "email" => FILTER_VALIDATE_EMAIL
    ];

    protected $user_type = [
        "administrator",
        "manager",
        "software_specialist",
        "customer",
        "guest"
    ];

	protected $required_fields = [
            "email",
            "password"
	];

    protected $constraints = [ 
        "email" => "unique",
        "password" => "confirmation"
    ];

	// TODO: implement construct and put the proper configurations from this Mode in it

	/**
	 * Constructor
	 *
     * @internal config file with json $config
     *
	 * @param array (
	 * 	'applicationid' => string
	 * 	'database' => string
	 * 	'report' => string
	 * 	'command' => string
	 * 	'username' => string
	 * 	'userpassword' => string
	 * )
     *
	 * @return $this instance
	 */
	public function __construct() {
	    $config_param['database'] = $this->database;

	    parent::__construct($config_param);
	}

    /**
     * Fill the Single Model
     * 
     * @param array $data
     * @return User
     */
    protected function fillModel(array $data){
        $user_instance = new User;

        foreach($data as $key => $value){
            $user_instance->{$key} = $value;
        }

        return $user_instance;
    }

	/**
	 * Find Page
	 *
	 * @param int $sisn
	 */
	public function find($id) {
        $result = $this->search(["id" => $id]);

        return $result;
	}

	/**
	 * 
	 */
	public function findAll(){
		$result = $this->search([]);
		
		return $result;
	}

    /**
     * @internal {search clusters section} is the part where we can search multiple fields using only one expression
     *
     * @param array $params
     */
    public function search(array $params, $recall = false)
    {
        // search clusters section
        if( isset($params["search"]) && !empty($params["search"]) ){
            $params['name'] = $params["search"];
            $params['email'] = $params["search"];
            $params['user_type'] = $params["search"];
            $params['updated_at'] = $params["search"];
            $params['logic'] = 1;
            unset($params["search"]);
        }

        $results = parent::searchRecords($params, $recall);

        $results->results = $this->_filterSchema($results->results);

        return $results;
    }

    /**
     * Register User
     *
     * @param array $input_data
     */
    public function register(array $input_data) {

       $input_data = $this->sanitizeInputData( $input_data );

       $validation_result = $this->validateInputData($input_data);

       if( !$validation_result['status'] ){

            switch( true ){
                case isset($_SESSION['missing_fields']):
                    $message = 'registration_missing_fields';
                    break;

                case isset($_SESSION['wrong_type_fields']):
                    $message = 'invalid_data_format';
                    break;
                default:
                    $message = 'unknown';
                    break;
           }

            return json_encode([
                'status' => false,
                'message' => $message,
                'message-detail' => $validation_result
            ]);
        }

        $input_data['password'] = md5($input_data['password']);

        $result = $this->save($input_data);

        return $result;

    }

    /**
     *
     */
    public function save(array $client_data)
    {
        // TODO: fix the edition to not replace password

        // encrypt md5
        if (isset($client_data['password'])) {
            $client_data['password'] = md5($client_data['password']);
        }

        if (isset($client_data['password_confirmation'])) {
            $client_data['password_confirmation'] = md5($client_data['password_confirmation']);
        }
        // --

        return parent::save($client_data);
    }

}
