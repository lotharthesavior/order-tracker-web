
# Masa CMS

@author Savio Resende <savio@savioresende.com.br>

Current branch: `websiteNavigationDetails`

### 1. Include Marina Component in your project

For this, you will need:

- Git installed in your machine, for more information about this, here is a good reference: https://git-scm.com/download/win
- Composer, reference: https://getcomposer.org/

### 2. Composer configuration

Reference for Composer: https://getcomposer.org/

Example of composer.json loading Models and Marina component:

```json
{
    "repositories": [
        {
            "url": "git@bitbucket.org:lotharthesavior/marina.git",
            "type": "vcs"
        }
    ],
    "require": {
        ...
        "savio/marina": "dev-master",
        ...
    },
    "autoload": {
        "psr-4": {
            "Marina\\": "Components/Marina/Components/",
            "Models\\": "Models/",
            "Controllers\\": "Controllers/"
        }
    }
    ...
}
```

Command line to run after every update on **composer.json** file:

```sh
composer install
composer dump-autoload
```

It require an installation of composer on windows environment in a global way. if you prefer the composer.phar your command line would be:


```sh
php composer.phar dump-autoload
```

### 3. Theme

The theme can be any, and you just put it into the directory **public**. It comes with the template SB Admin 2 (https://startbootstrap.com/template-overviews/sb-admin-2/). It uses Bootstrap and looks Fancy. =)


### 4. Slim Micro Framework

This framework was choosen because of it's simplicity. The web is just Request / Response. And this tool achieve exactly this. From it's own documentation: "At its core, Slim is a dispatcher that receives an HTTP request, invokes an appropriate callback routine, and returns an HTTP response. That’s it."

Reference: https://www.slimframework.com


### 5. Configuring the default Client ID

The application, on the Authorization moment, must define a main user for the login. This is like this because the ID of the client logging in must the defined at some point.

**TODO**: find a better solution for this authorization problem (having to store the users table in a main client section)