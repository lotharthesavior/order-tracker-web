<?php

$logged_user = "guest";

if(
    isset($_SESSION['user_logon'])
    && !empty($_SESSION['user_logon']["USER_TYPE"])
){

	$logged_user = strtolower($_SESSION['user_logon']["USER_TYPE"]);

} else if (isset($_COOKIE["resendes-house-persistent"])) {

    $user_credentials = json_decode(hex2bin($_COOKIE["resendes-house-persistent"]), true);
    $_SESSION['user_logon'] = $user_credentials;

}

$permissions_list = [
    "resources" => [
        "/",
        // tickets -------------
        "/tickets",
        '/ticket/{id}',
        "/ticketsedit",
        '/ticketsedit/{id}',
        '/ticket-delete/{id}',
        '/tickets-comments/{ticket}',
        '/tickets-comment-edit',
        '/tracker-test-edit',
        '/tracker-fix-edit',
        '/tickets-responsible-edit',
        '/tickets-comment-delete/{id}',
        '/tracker-fix-delete/{id}',
        '/removeOccurrence',
        // users ---------------
        "/users",
        '/user/{id}',
        "/usersedit",
        '/usersedit/{id}',
        '/user-delete/{id}',
        // patron ---------------
        '/patron',
        // client ---------------
        '/client',
        // release ---------------
        '/release',
        // fix ---------------
        '/fix/{id}',
        // other ---------------
        "/documentation",
        "/documentation/{id}",
        "/software-updates",
        "/resources",
        "/news-events",
        "/our-history",
        "/suggestion-post",
        "/file-upload",
        "/uploads-db/{database}/{record}/{file}/{extension}",
        // auth -----------------
        "/login",
        "/register",
        "/logout",
        "/not-authorized",
        "/forget-password",
        "/new-password"
    ],

    "roles" => [
        "administrator",
        "manager",
        "software_specialist",
        "customer",
        "guest"
    ],

    "routes" => [
        ['/login', ['administrator', 'manager', 'software_specialist', 'customer', 'guest'], ['get', 'post']],
        ['/logout', ['administrator', 'manager', 'software_specialist', 'customer', 'guest'], ['get']],
        ['/ticketsedit', ['administrator'], ['get', 'post']],
        ['/ticket-delete/{id}', ['administrator'], ['get']],
        ['/tickets-comment-edit', ['administrator'], ['get', 'post']],
        ['/register', ['administrator', 'manager', 'software_specialist', 'customer', 'guest'], ['get', 'post']],
        ['/new-password', ['administrator', 'manager', 'software_specialist', 'customer', 'guest'], ['get', 'post']],
        ['/forget-password', ['administrator', 'manager', 'software_specialist', 'customer', 'guest'], ['get', 'post']],
        ['/file-upload', ['administrator'], ['post']],
        ["/uploads-db/{database}/{record}/{file}/{extension}", ['administrator', 'manager', 'software_specialist', 'customer'], ['get']],
        // users ---------------
        ['/usersedit', ['administrator'], ['get', 'post']],
    ],

    "assignments" => [
        "allow" => [

            "manager" => [
                // other -----------------
                "/",
                "/documentation",
                "/documentation/{id}",
                "/resources",
                "/news-events",
                "/our-history",
                "/suggestion-post",
                "/file-upload",
                "/uploads-db/{database}/{record}/{file}/{extension}",
                // tickets ---------------
                "/tickets",
                '/ticket/{id}',
                "/ticketsedit",
                '/ticketsedit/{id}',
                '/ticket-delete/{id}',
                '/tickets-comments/{ticket}',
                '/tickets-comment-edit',
                '/tracker-test-edit',
                '/tracker-fix-edit',
                '/tickets-comment-delete/{id}',
                '/tracker-fix-delete/{id}',
                '/removeOccurrence',
                // patron ---------------
                '/patron',
                // client ---------------
                '/client',
                // release ---------------
                '/release',
                // fix ---------------
                '/fix/{id}',
                // auth ------------------
                "/login",
                "/register",
                "/new-password",
                "/logout",
                "/not-authorized",
                "/forget-password"
            ],

            "software_specialist" => [
                // other -----------------
                "/",
                "/documentation",
                "/documentation/{id}",
                "/resources",
                "/news-events",
                "/our-history",
                "/suggestion-post",
                "/file-upload",
                "/uploads-db/{database}/{record}/{file}/{extension}",
                // auth ------------------
                "/login",
                "/register",
                "/logout",
                "/not-authorized",
                "/forget-password",
                "/new-password",
                // tickets ---------------
                "/tickets",
                '/ticket/{id}',
                "/ticketsedit",
                '/ticketsedit/{id}',
                '/ticket-delete/{id}',
                '/tickets-comments/{ticket}',
                '/tickets-comment-edit',
                '/tracker-test-edit',
                '/tracker-fix-edit',
                '/tickets-comment-delete/{id}',
                '/tracker-fix-delete/{id}',
                '/removeOccurrence',
                // patron ---------------
                '/patron',
                // release ---------------
                '/release',
                // fix ---------------
                '/fix/{id}',
                // client ---------------
                '/client'
            ],

            "customer" => [
                // other -----------------
                "/",
                "/documentation",
                "/documentation/{id}",
                "/software-updates",
                "/resources",
                "/news-events",
                "/our-history",
                "/suggestion-post",
                "/file-upload",
                "/uploads-db/{database}/{record}/{file}/{extension}",
                // auth ------------------
                "/login",
                "/register",
                "/new-password",
                "/logout",
                "/not-authorized",
                "/forget-password",
                // tickets ---------------
                "/tickets",
                '/ticket/{id}',
                "/ticketsedit",
                '/ticketsedit/{id}',
                '/ticket-delete/{id}',
                '/tickets-comments/{ticket}',
                '/tickets-comment-edit',
                '/tracker-test-edit',
                '/tracker-fix-edit',
                '/tickets-comment-delete/{id}',
                '/tracker-fix-delete/{id}',
                '/removeOccurrence',
                // patron ---------------
                '/patron',
                // release ---------------
                '/release',
                // fix ---------------
                '/fix/{id}',
            ],

            "administrator" => [
                // other -----------------
                "/",
                "/documentation",
                "/documentation/{id}",
                "/software-updates",
                "/resources",
                "/news-events",
                "/our-history",
                "/suggestion-post",
                "/file-upload",
                "/uploads-db/{database}/{record}/{file}/{extension}",
                // tickets ---------------
                "/tickets",
                '/ticket/{id}',
                "/ticketsedit",
                '/ticketsedit/{id}',
                '/ticket-delete/{id}',
                '/tickets-comments/{ticket}',
                '/tickets-comment-edit',
                '/tracker-test-edit',
                '/tracker-fix-edit',
                '/tickets-responsible-edit',
                '/tickets-comment-delete/{id}',
                '/tracker-fix-delete/{id}',
                '/removeOccurrence',
                // users ---------------
                "/users",
                '/user/{id}',
                "/usersedit",
                '/usersedit/{id}',
                '/user-delete/{id}',
                // auth ------------------
                "/login",
                "/register",
                "/new-password",
                "/logout",
                "/not-authorized",
                "/forget-password",
                // patron ---------------
                '/patron',
                // release ---------------
                '/release',
                // fix ---------------
                '/fix/{id}',
                // client ---------------
                "/login",
                '/client'
            ],
            "guest" => [
                "/not-authorized",
                "/login",
                "/register",
                "/new-password",
                "/forget-password"
            ]
        ],
        "deny" => [
            // "guest" => ["/no", "/yes"],
            // "user1" => ["/yes"],
            // "user2" => ["/no"]
        ]
    ]
];

// Auto Generated permissions -----------------------------------
foreach (glob("routes/*-acl-routes.php") as $route_file) {
    include $route_file;
}
// ---------------------------------------------------------

$app->add(new \MasaAcl\MasaAcl([$logged_user], $permissions_list));
