<?php
/**
 *
 */

require __DIR__ . '/vendor/autoload.php';

use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local;

class CrudBuilder
{
    protected $adapter;
    protected $filesystem;

    protected $database_capitalize_plural;
    protected $database_capitalize_singular;
    protected $database_lowercase_plural;
    protected $database_lowercase_singular;
    protected $database_fields;
    protected $database_required_fields;
    protected $template_dir_name;
    protected $fields_table;

    /**
     * CrudBuilder constructor.
     *
     * @param string $database_capitalize_plural (e.g. Notes, replaced by {{database-capitalize-plural}})
     * @param string $database_capitalize_singular (e.g. Note, replaced by {{database-capitalize-singular}})
     * @param $database_lowercase_plural (e.g. notes, replaced by {{database-lowercase-plural}})
     * @param $database_lowercase_singular (e.g. note, replaced by {{database-lowercase-singular}})
     * @param array $database_fields (replaced by, {{database-fields}} or {{protected-model-database-fields}})
     *        e.g.: $database_fields = [
     *                  'title' => 'string',
     *                  'content' => 'string'
     *              ];
     * @param array $database_required_fields (replaced by {{database-required-fields}}) (TODO)
     * @param string $template_dir_name (e.g. sb-admin)
     * @param array $fields_table (replaced by {{table-column-labels}} or {{table-column-values}})
     *        e.g.: $fields_table = [
     *                  'Id' => [
     *                      'field_name' => 'id',
     *                      'type' => 'input-hidden',
     *                      'attributes' => ''
     *                  ],
     *                  'Title' => [
     *                      'field_name' => 'title',
     *                      'type' => 'input-text',
     *                      'attributes' => 'maxlength="60"'
     *                  ],
     *                  'Content' => [
     *                      'list' => false,
     *                      'field_name' => 'content',
     *                      'type' => 'textarea',
     *                      'attributes' => ''
     *                  ]
     *              ];
     */
    public function __construct(
        $database_capitalize_plural,
        $database_capitalize_singular,
        $database_lowercase_plural,
        $database_lowercase_singular,
        $database_fields,
        $database_required_fields,
        $template_dir_name,
        $fields_table
    )
    {
        $this->database_capitalize_plural   = $database_capitalize_plural;
        $this->database_capitalize_singular = $database_capitalize_singular;
        $this->database_lowercase_plural    = $database_lowercase_plural;
        $this->database_lowercase_singular  = $database_lowercase_singular;
        $this->database_fields              = $database_fields;
        $this->database_required_fields     = $database_required_fields;
        $this->template_dir_name            = $template_dir_name;
        $this->fields_table                 = $fields_table;

        $this->adapter = new Local(__DIR__.'/');
        $this->filesystem = new Filesystem($this->adapter);
    }

    /**
     * Create Views
     *
     */
    public function createViews()
    {
        $this->createDatabaseHomeView();
        $this->createDatabaseEditView();
        $this->createDatabaseView();
        $this->createDatabaseFormView();
        $this->createDatabaseTableView();
        $this->createDatabaseRecordView();
    }

    /**
     * Step 1: create database home view
     *
     * @internal database.php
     */
    private function createDatabaseHomeView()
    {
        $step_front_1_result = '';

        $template_database_file = file_get_contents('structure_files/views/database.php');

        // database name
        $template_database_file = str_replace('{{database-capitalize-singular}}', $this->database_capitalize_singular, $template_database_file);
        $template_database_file = str_replace('{{database-capitalize-plural}}', $this->database_capitalize_plural, $template_database_file);
        $template_database_file = str_replace('{{database-lowercase-plural}}', $this->database_lowercase_plural, $template_database_file);
        $template_database_file = str_replace('{{database-lowercase-singular}}', $this->database_lowercase_singular, $template_database_file);
        // --

        // $filesystem
        $template_database_address = 'public/' . $this->template_dir_name . '/' . $this->database_lowercase_plural . '.php';

        $step_front_1_result = '<p><span style="color:red">Warning:</span> public/template-dir/database.php not created. It already exists!</p>';
        if (!$this->filesystem->has($template_database_address)) {
            $this->filesystem->write($template_database_address, $template_database_file);
            $step_front_1_result = '<p><span style="color:green">Success:</span> public/template-dir/database.php created!</p>';
        }
        // --

        echo $step_front_1_result;
    }

    /**
     * Step 2: create database edit view
     *
     * @internal database-edit.php
     */
    private function createDatabaseEditView()
    {
        // database-edit.php --
        $step_front_2_result = '';

        $template_database_edit_file = file_get_contents('structure_files/views/database-edit.php');

        // database name
        $template_database_edit_file = str_replace('{{database-capitalize-singular}}', $this->database_capitalize_singular, $template_database_edit_file);
        $template_database_edit_file = str_replace('{{database-capitalize-plural}}', $this->database_capitalize_plural, $template_database_edit_file);
        $template_database_edit_file = str_replace('{{database-lowercase-plural}}', $this->database_lowercase_plural, $template_database_edit_file);
        $template_database_edit_file = str_replace('{{database-lowercase-singular}}', $this->database_lowercase_singular, $template_database_edit_file);
        // --

        // $filesystem
        $template_database_edit_address = 'public/' . $this->template_dir_name . '/' . $this->database_lowercase_plural . '-edit.php';

        $step_front_2_result = '<p><span style="color:red">Warning:</span> public/template-dir/database-edit.php not created. It already exists!</p>';
        if (!$this->filesystem->has($template_database_edit_address)) {
            $this->filesystem->write($template_database_edit_address, $template_database_edit_file);
            $step_front_2_result = '<p><span style="color:green">Success:</span> public/template-dir/database-edit.php created!</p>';
        }
        // --

        echo $step_front_2_result;
        // --
    }

    /**
     * Step 3: create database view
     *
     * @internal database-view.php
     */
    private function createDatabaseView()
    {
        // database-view.php --
        $step_front_3_result = '';

        $template_database_view_file = file_get_contents('structure_files/views/database-view.php');

        // Controller name
        $template_database_view_file = str_replace('{{database-capitalize-singular}}', $this->database_capitalize_singular, $template_database_view_file);
        $template_database_view_file = str_replace('{{database-capitalize-plural}}', $this->database_capitalize_plural, $template_database_view_file);
        $template_database_view_file = str_replace('{{database-lowercase-plural}}', $this->database_lowercase_plural, $template_database_view_file);
        $template_database_view_file = str_replace('{{database-lowercase-singular}}', $this->database_lowercase_singular, $template_database_view_file);
        // --

        // $this->filesystem
        $template_database_view_address = 'public/' . $this->template_dir_name . '/' . $this->database_lowercase_plural . '-view.php';

        // fields
        $template_field = '';
        foreach ($this->fields_table as $field_key => $field_value) {
            $template_field_content = file_get_contents('structure_files/views/includes/database/database-view-fields/text.php');
            $template_field_content = str_replace('{{database-lowercase-singular}}', $this->database_lowercase_singular, $template_field_content);
            $template_field_content = str_replace('{{field-id}}', $field_value['field_name'], $template_field_content);
            $template_field_content = str_replace('{{field-input-attributes}}', $field_value['attributes'], $template_field_content);
            $template_field .= $template_field_content;
        }
        $template_database_view_file = str_replace('{{database-form-fields}}', $template_field, $template_database_view_file);
        // --

        $step_front_3_result = '<p><span style="color:red">Warning:</span> public/template-dir/database-view.php not created. It already exists!</p>';
        if (!$this->filesystem->has($template_database_view_address)) {
            $this->filesystem->write($template_database_view_address, $template_database_view_file);
            $step_front_3_result = '<p><span style="color:green">Success:</span> public/template-dir/database-view.php created!</p>';
        }
        // --

        echo $step_front_3_result;
        // --
    }

    /**
     * Step 4: create
     *
     * @internal database-form.php
     */
    private function createDatabaseFormView()
    {
        // includes/users/database-form.php --
        $step_4_result = '';

        $template_database_include_form_file = file_get_contents('structure_files/views/includes/database/database-form.php');

        // database name
        $template_database_include_form_file = str_replace('{{database-capitalize-singular}}', $this->database_capitalize_singular, $template_database_include_form_file);
        $template_database_include_form_file = str_replace('{{database-capitalize-plural}}', $this->database_capitalize_plural, $template_database_include_form_file);
        $template_database_include_form_file = str_replace('{{database-lowercase-plural}}', $this->database_lowercase_plural, $template_database_include_form_file);
        $template_database_include_form_file = str_replace('{{database-lowercase-singular}}', $this->database_lowercase_singular, $template_database_include_form_file);
        // --

        // fields
        $template_field = '';
        foreach ($this->fields_table as $field_key => $field_value) {
            $template_field_content = file_get_contents('structure_files/views/includes/database/database-form-fields/' . $field_value['type'] . '.php');
            $template_field_content = str_replace('{{database-lowercase-singular}}', $this->database_lowercase_singular, $template_field_content);
            $template_field_content = str_replace('{{field-id}}', $field_value['field_name'], $template_field_content);
            $template_field_content = str_replace('{{field-label}}', $field_key, $template_field_content);
            $template_field_content = str_replace('{{field-input-attributes}}', $field_value['attributes'], $template_field_content);
            $template_field .= $template_field_content;
        }
        $template_database_include_form_file = str_replace('{{database-form-fields}}', $template_field, $template_database_include_form_file);
        // --

        // $this->filesystem
        $template_database_include_form_address = 'public/' . $this->template_dir_name . '/includes/' . $this->database_lowercase_plural . '/' . $this->database_lowercase_plural . '-form.php';

        $step_4_result = '<p><span style="color:red">Warning:</span> public/template-dir/includes/database/database-form.php not created. It already exists!</p>';
        if (!$this->filesystem->has($template_database_include_form_address)) {
            $this->filesystem->write($template_database_include_form_address, $template_database_include_form_file);
            $step_4_result = '<p><span style="color:green">Success:</span> public/template-dir/includes/database/database-form.php created!</p>';
        }

        echo $step_4_result;
        // --
    }

    /**
     * Step 5: Create database table view
     *
     * @internal table.php
     */
    private function createDatabaseTableView()
    {
        // includes/users/table.php --
        $step_5_result = '';

        $template_database_include_table_file = file_get_contents('structure_files/views/includes/database/table.php');

        // database name
        $template_database_include_table_file = str_replace('{{database-capitalize-singular}}', $this->database_capitalize_singular, $template_database_include_table_file);
        $template_database_include_table_file = str_replace('{{database-capitalize-plural}}', $this->database_capitalize_plural, $template_database_include_table_file);
        $template_database_include_table_file = str_replace('{{database-lowercase-plural}}', $this->database_lowercase_plural, $template_database_include_table_file);
        $template_database_include_table_file = str_replace('{{database-lowercase-singular}}', $this->database_lowercase_singular, $template_database_include_table_file);
        // --

        // fields to be shown on table header
        $table_column_labels = PHP_EOL;
        foreach ($this->fields_table as $field_key => $field_value) {
            if (isset($field_value['list']) && !$field_value['list']) {
                continue;
            }

            $table_column_labels .= '<th>' . $field_key . '</th>' . PHP_EOL;
        }
        $template_database_include_table_file = str_replace('{{table-column-labels}}', $table_column_labels, $template_database_include_table_file);
        // --

        // fields to be shown on table body
        $table_column_values = PHP_EOL;
        foreach ($this->fields_table as $field_key => $field_value) {
            if (isset($field_value['list']) && !$field_value['list']) {
                continue;
            }

            if ($field_key === 'Id') {
                $table_column_values .= '<td><a href="/' . $this->database_lowercase_plural . 'edit/<?php echo $value["id"]; ?>"><?php echo $value["' . $field_value['field_name'] . '"]; ?></a></td>' . PHP_EOL;
            } else {
                $table_column_values .= '<td><a href="/' . $this->database_lowercase_plural . 'edit/<?php echo $value["id"]; ?>"><?php echo $value["file_content"]["' . $field_value['field_name'] . '"]; ?></a></td>' . PHP_EOL;
            }
        }
        $template_database_include_table_file = str_replace('{{table-column-values}}', $table_column_values, $template_database_include_table_file);
        // --

        // $this->filesystem
        $template_database_include_table_address = 'public/' . $this->template_dir_name . '/includes/' . $this->database_lowercase_plural . '/table.php';

        $step_5_result = '<p><span style="color:red">Warning:</span> public/template-dir/includes/database/table.php not created. It already exists!</p>';
        if (!$this->filesystem->has($template_database_include_table_address)) {
            $this->filesystem->write($template_database_include_table_address, $template_database_include_table_file);
            $step_5_result = '<p><span style="color:green">Success:</span> public/template-dir/includes/database/table.php created!</p>';
        }

        echo $step_5_result;
        // --
    }

    /**
     * Step 6: Create the record view.
     *
     * @internal includes/database/view.php
     */
    private function createDatabaseRecordView()
    {
        // includes/users/view.php
        $step_6_result = '';

        $template_database_include_view_file = file_get_contents('structure_files/views/includes/database/view.php');

        // database name
        $template_database_include_view_file = str_replace('{{database-capitalize-singular}}', $this->database_capitalize_singular, $template_database_include_view_file);
        $template_database_include_view_file = str_replace('{{database-capitalize-plural}}', $this->database_capitalize_plural, $template_database_include_view_file);
        $template_database_include_view_file = str_replace('{{database-lowercase-plural}}', $this->database_lowercase_plural, $template_database_include_view_file);
        $template_database_include_view_file = str_replace('{{database-lowercase-singular}}', $this->database_lowercase_singular, $template_database_include_view_file);
        // --

        // fields
        $template_field = '';
        foreach ($this->fields_table as $field_key => $field_value) {
            $template_field_content = file_get_contents('structure_files/views/includes/database/database-view-fields/text.php');
            $template_field_content = str_replace('{{database-lowercase-singular}}', $this->database_lowercase_singular, $template_field_content);
            $template_field_content = str_replace('{{field-id}}', $field_value['field_name'], $template_field_content);
            $template_field_content = str_replace('{{field-label}}', $field_key, $template_field_content);
            $template_field_content = str_replace('{{field-input-attributes}}', $field_value['attributes'], $template_field_content);
            $template_field .= $template_field_content;
        }
        $template_database_include_view_file = str_replace('{{database-form-fields}}', $template_field, $template_database_include_view_file);
        // --

        // $this->filesystem
        $template_database_include_view_address = 'public/' . $this->template_dir_name . '/includes/' . $this->database_lowercase_plural . '/view.php';

        $step_6_result = '<p><span style="color:red">Warning:</span> public/template-dir/includes/database/view.php not created. It already exists!</p>';
        if (!$this->filesystem->has($template_database_include_view_address)) {
            $this->filesystem->write($template_database_include_view_address, $template_database_include_view_file);
            $step_6_result = '<p><span style="color:green">Success:</span> public/template-dir/includes/database/view.php created!</p>';
        }

        echo $step_6_result;
    }

    // ###################
    // ###################
    // ###################
    // ###################
    // ###################
    // ###################
    // ###################
    // ###################
    // ###################

    /**
     * Create Models
     */
    public function createModels()
    {
        $this->createDatabaseModel();
        $this->CreateSingularModel();
    }

    /**
     * Step 7: Create Database Model
     *
     * @interla Database.php
     */
    private function createDatabaseModel()
    {
        $step_1_result = '';

        $database_collection_file = file_get_contents('structure_files/Models/Database.php');

        // database name
        $database_collection_file = str_replace('{{database-capitalize-singular}}', $this->database_capitalize_singular, $database_collection_file);
        $database_collection_file = str_replace('{{database-capitalize-plural}}', $this->database_capitalize_plural, $database_collection_file);
        $database_collection_file = str_replace('{{database-lowercase-plural}}', $this->database_lowercase_plural, $database_collection_file);
        $database_collection_file = str_replace('{{database-lowercase-singular}}', $this->database_lowercase_singular, $database_collection_file);
        // --

        // database-fields
        // protected fields
        $protected_model_database_fields = PHP_EOL;
        foreach ($this->database_fields as $field_key => $field_type) {
            $protected_model_database_fields .= 'protected $_' . $field_key . ';';
            $protected_model_database_fields .= PHP_EOL;
        }
        $database_collection_file = str_replace('{{protected-model-database-fields}}', $protected_model_database_fields, $database_collection_file);

        // fields list
        $protected_model_fields_list = '';
        foreach ($this->database_fields as $field_key => $field_type) {
            $protected_model_fields_list .= '"' . $field_key . '",';
            $protected_model_fields_list .= PHP_EOL;
        }
        $database_collection_file = str_replace('{{database-fields}}', $protected_model_fields_list, $database_collection_file);

        // required fields (TODO)
        $database_collection_file = str_replace('{{database-required-fields}}', '', $database_collection_file);

        // constraints (TODO)
        $database_collection_file = str_replace('{{database-constraints}}', '', $database_collection_file);
        // --

        // $this->filesystem
        $database_model_address = 'Models/Generated/' . $this->database_capitalize_plural . '.php';

        $step_1_result = '<p><span style="color:red">Warning:</span> (Model) Database.php not created. It already exists!</p>';
        if (!$this->filesystem->has($database_model_address)) {
            $this->filesystem->write($database_model_address, $database_collection_file);
            $step_1_result = '<p><span style="color:green">Success:</span> (Model) Database.php created!</p>';
        }
        // --

        echo $step_1_result;
    }

    /**
     * Step 8: Create Singular Model
     *
     * @internal DB/Database-singular.php
     */
    private function CreateSingularModel()
    {
        $step_2_result = '';

        $database_singular_file = file_get_contents('structure_files/Models/DB/Database-singular.php');

        // database name
        $database_singular_file = str_replace('{{database-capitalize-singular}}', $this->database_capitalize_singular, $database_singular_file);
        $database_singular_file = str_replace('{{database-capitalize-plural}}', $this->database_capitalize_plural, $database_singular_file);
        $database_singular_file = str_replace('{{database-lowercase-plural}}', $this->database_lowercase_plural, $database_singular_file);
        $database_singular_file = str_replace('{{database-lowercase-singular}}', $this->database_lowercase_singular, $database_singular_file);
        // --

        // database-fields
        $protected_model_database_fields = PHP_EOL;
        foreach ($this->database_fields as $field_key => $field_type) {
            $protected_model_database_fields .= 'protected $_' . $field_key . ';';
            $protected_model_database_fields .= PHP_EOL;
        }
        // --

        $database_singular_file = str_replace('{{protected-model-database-fields}}', $protected_model_database_fields, $database_singular_file);

        // $this->filesystem
        $database_singular_model_address = 'Models/DB/Generated/' . $this->database_capitalize_singular . '.php';

        $step_2_result = '<p><span style="color:red">Warning:</span> (Model) DB/Database-singular.php not created. It already exists!</p>';
        if (!$this->filesystem->has($database_singular_model_address)) {
            $this->filesystem->write($database_singular_model_address, $database_singular_file);
            $step_2_result = '<p><span style="color:green">Success:</span> (Model) Database-singular.php created!</p>';
        }
        // --

        echo $step_2_result;
    }

    /**
     * Create Controller
     *
     * @internal DatabaseController.php
     */
    public function createController()
    {
        $step_3_result = '';

        $controller_file = file_get_contents('structure_files/Controllers/DatabaseController.php');

        // Controller name
        $controller_file = str_replace('{{database-capitalize-singular}}', $this->database_capitalize_singular, $controller_file);
        $controller_file = str_replace('{{database-capitalize-plural}}', $this->database_capitalize_plural, $controller_file);
        $controller_file = str_replace('{{database-lowercase-plural}}', $this->database_lowercase_plural, $controller_file);
        $controller_file = str_replace('{{database-lowercase-singular}}', $this->database_lowercase_singular, $controller_file);
        // --

        // $this->filesystem
        $database_controller_address = 'Controllers/Generated/' . $this->database_capitalize_plural . 'Controller.php';

        $step_3_result = '<p><span style="color:red">Warning:</span> (Controller) DatabaseController.php not created. It already exists!</p>';
        if (!$this->filesystem->has($database_controller_address)) {
            $this->filesystem->write($database_controller_address, $controller_file);
            $step_3_result = '<p><span style="color:green">Success:</span> (Controller) DatabaseController.php created!</p>';
        }
        // --

        echo $step_3_result;
    }

    /**
     * Create Routes
     *
     * @internal routes/database-routes.php
     */
    public function createDatabaseRoutes()
    {
        $step_4_result = '';

        $routes_file = file_get_contents('structure_files/views/routes.php');

        // routes values
        $routes_file = str_replace('{{database-capitalize-singular}}', $this->database_capitalize_singular, $routes_file);
        $routes_file = str_replace('{{database-capitalize-plural}}', $this->database_capitalize_plural, $routes_file);
        $routes_file = str_replace('{{database-lowercase-plural}}', $this->database_lowercase_plural, $routes_file);
        $routes_file = str_replace('{{database-lowercase-singular}}', $this->database_lowercase_singular, $routes_file);
        // --

        // create route file
        $database_routes_address = 'routes/' . $this->database_lowercase_plural . '-routes.php';
        // $routes_file
        $step_4_result = '<p><span style="color:red">Warning:</span> database-routes.php not created. It already exists!</p>';
        if (!$this->filesystem->has($database_routes_address)) {
            $this->filesystem->write($database_routes_address, $routes_file);
            $step_4_result = '<p><span style="color:green">Success:</span> database-routes.php created!</p>';
        }
        // --

        echo $step_4_result;
    }

    /**
     * Create Navigation
     *
     * @internal database-navigation.php
     */
    public function createDatabaseNavigation()
    {
        $step_5_result = '';
        $navigation_file = file_get_contents('structure_files/views/database-navigation.php');

        // routes values
        $navigation_file = str_replace('{{database-capitalize-singular}}', $this->database_capitalize_singular, $navigation_file);
        $navigation_file = str_replace('{{database-capitalize-plural}}', $this->database_capitalize_plural, $navigation_file);
        $navigation_file = str_replace('{{database-lowercase-plural}}', $this->database_lowercase_plural, $navigation_file);
        $navigation_file = str_replace('{{database-lowercase-singular}}', $this->database_lowercase_singular, $navigation_file);
        // --

        // create route file
        $database_navigation_address = 'public/' . $this->template_dir_name . '/includes/navigations/' . $this->database_lowercase_plural . '-navigation.php';
        // $navigation_file
        $step_5_result = '<p><span style="color:red">Warning:</span> database-navigation.php not created. It already exists!</p>';
        if (!$this->filesystem->has($database_navigation_address)) {
            $this->filesystem->write($database_navigation_address, $navigation_file);
            $step_5_result = '<p><span style="color:green">Success:</span> database-navigation.php created!</p>';
        }
        // --

        echo $step_5_result;
    }

    /**
     * Create ACL Routes
     *
     * @internal acl-routes.php
     */
    public function createAclRoutes()
    {
        $step_6_result = '';
        $acl_routes_file = file_get_contents('structure_files/views/acl-routes.php');

        // routes values
        $acl_routes_file = str_replace('{{database-capitalize-singular}}', $this->database_capitalize_singular, $acl_routes_file);
        $acl_routes_file = str_replace('{{database-capitalize-plural}}', $this->database_capitalize_plural, $acl_routes_file);
        $acl_routes_file = str_replace('{{database-lowercase-plural}}', $this->database_lowercase_plural, $acl_routes_file);
        $acl_routes_file = str_replace('{{database-lowercase-singular}}', $this->database_lowercase_singular, $acl_routes_file);
        // --

        // create route file
        $database_acl_routes_address = 'routes/' . $this->database_lowercase_plural . '-acl-routes.php';
        // $acl_routes_file
        $step_6_result = '<p><span style="color:red">Warning:</span> database-acl-routes.php not created. It already exists!</p>';
        if (!$this->filesystem->has($database_acl_routes_address)) {
            $this->filesystem->write($database_acl_routes_address, $acl_routes_file);
            $step_6_result = '<p><span style="color:green">Success:</span> database-acl-routes.php created!</p>';
        }
        // --

        echo $step_6_result;
    }

    /**
     * Create Containers
     */
    public function createDatabaseContainers()
    {
        $this->createDatabaseControllerContainers();
        $this->createDatabaseModelContainer();
        $this->createModelSingularContainer();
    }

    /**
     * Create Controller Container
     *
     * @internal DatabaseController-container.php
     */
    private function createDatabaseControllerContainers()
    {
        // create controllers container --
        $step_7_result = '';
        $controller_container_file = file_get_contents('structure_files/Controllers/DatabaseController-container.php');

        // routes values
        $controller_container_file = str_replace('{{database-capitalize-singular}}', $this->database_capitalize_singular, $controller_container_file);
        $controller_container_file = str_replace('{{database-capitalize-plural}}', $this->database_capitalize_plural, $controller_container_file);
        $controller_container_file = str_replace('{{database-lowercase-plural}}', $this->database_lowercase_plural, $controller_container_file);
        $controller_container_file = str_replace('{{database-lowercase-singular}}', $this->database_lowercase_singular, $controller_container_file);
        // --

        // create route file
        $database_controller_container_address = 'Controllers/Generated/' . $this->database_lowercase_plural . '-container.php';
        // $controller_container_file
        $step_7_result = '<p><span style="color:red">Warning:</span> DatabaseController-container.php not created. It already exists!</p>';
        if (!$this->filesystem->has($database_controller_container_address)) {
            $this->filesystem->write($database_controller_container_address, $controller_container_file);
            $step_7_result = '<p><span style="color:green">Success:</span> DatabaseController-container.php created!</p>';
        }
        // --

        echo $step_7_result;
    }

    /**
     * Create Model container
     *
     * @internal Database-container.php
     */
    private function createDatabaseModelContainer()
    {
        // create model container --
        $step_8_result = '';
        $database_model_container_file = file_get_contents('structure_files/Models/Database-container.php');

        // routes values
        $database_model_container_file = str_replace('{{database-capitalize-singular}}', $this->database_capitalize_singular, $database_model_container_file);
        $database_model_container_file = str_replace('{{database-capitalize-plural}}', $this->database_capitalize_plural, $database_model_container_file);
        $database_model_container_file = str_replace('{{database-lowercase-plural}}', $this->database_lowercase_plural, $database_model_container_file);
        $database_model_container_file = str_replace('{{database-lowercase-singular}}', $this->database_lowercase_singular, $database_model_container_file);
        // --

        // create route file
        $database_model_container_address = 'Models/Generated/' . $this->database_lowercase_plural . '-container.php';
        // $database_model_container_file
        $step_8_result = '<p><span style="color:red">Warning:</span> Database-container.php not created. It already exists!</p>';
        if (!$this->filesystem->has($database_model_container_address)) {
            $this->filesystem->write($database_model_container_address, $database_model_container_file);
            $step_8_result = '<p><span style="color:green">Success:</span> Database-container.php created!</p>';
        }
        // --

        echo $step_8_result;
        // --
    }

    /**
     * Create Database Model Singular Container
     *
     * @internal DB/Database-singular-container.php
     */
    private function createModelSingularContainer()
    {
        // create model singular container --
        $step_9_result = '';
        $database_model_singular_container_file = file_get_contents('structure_files/Models/DB/Database-singular-container.php');

        // routes values
        $database_model_singular_container_file = str_replace('{{database-capitalize-singular}}', $this->database_capitalize_singular, $database_model_singular_container_file);
        $database_model_singular_container_file = str_replace('{{database-capitalize-plural}}', $this->database_capitalize_plural, $database_model_singular_container_file);
        $database_model_singular_container_file = str_replace('{{database-lowercase-plural}}', $this->database_lowercase_plural, $database_model_singular_container_file);
        $database_model_singular_container_file = str_replace('{{database-lowercase-singular}}', $this->database_lowercase_singular, $database_model_singular_container_file);
        // --

        // create route file
        $database_model_singular_container_address = 'Models/DB/Generated/' . $this->database_lowercase_singular . '-container.php';
        // database_model_singular_container_file
        $step_9_result = '<p><span style="color:red">Warning:</span> Database-singular-container.php not created. It already exists!</p>';
        if (!$this->filesystem->has($database_model_singular_container_address)) {
            $this->filesystem->write($database_model_singular_container_address, $database_model_singular_container_file);
            $step_9_result = '<p><span style="color:green">Success:</span> Database-singular-container.php created!</p>';
        }
        // --

        echo $step_9_result;
        // --
    }
} // end class


// ##################################
// Inputs
// ##################################
if (!isset($_POST['database_capitalize_plural'])) {
    echo "<div><span style='color:red;'>Missing Field:</span> database_capitalize_plural.</div>";exit;
}
$database_capitalize_plural = $_POST['database_capitalize_plural'];

if (!isset($_POST['database_capitalize_singular'])) {
    echo "<div><span style='color:red;'>Missing Field:</span> database_capitalize_singular.</div>";exit;
}
$database_capitalize_singular = $_POST['database_capitalize_singular'];

if (!isset($_POST['database_lowercase_plural'])) {
    echo "<div><span style='color:red;'>Missing Field:</span> database_lowercase_plural.</div>";exit;
}
$database_lowercase_plural = $_POST['database_lowercase_plural'];

if (!isset($_POST['database_lowercase_singular'])) {
    echo "<div><span style='color:red;'>Missing Field:</span> database_lowercase_singular.</div>";exit;
}
$database_lowercase_singular = $_POST['database_lowercase_singular'];

if (!isset($_POST['database_fields'])) {
    echo "<div><span style='color:red;'>Missing Field:</span> database_fields.</div>";exit;
}
$database_fields = json_decode($_POST['database_fields'], true);

if (!isset($_POST['database_required_fields'])) {
    echo "<div><span style='color:red;'>Missing Field:</span> database_required_fields.</div>";exit;
}
$database_required_fields = $_POST['database_required_fields'];

if (!isset($_POST['template_dir_name'])) {
    echo "<div><span style='color:red;'>Missing Field:</span> template_dir_name.</div>";exit;
}
$template_dir_name = $_POST['template_dir_name'];

if (!isset($_POST['fields_table'])) {
    echo "<div><span style='color:red;'>Missing Field:</span> fields_table.</div>";exit;
}
$fields_table = json_decode($_POST['fields_table'], true);
// ##############################


// ##############################
// Execution
// ##############################
$CrudCreator = new CrudBuilder(
    $database_capitalize_plural,
    $database_capitalize_singular,
    $database_lowercase_plural,
    $database_lowercase_singular,
    $database_fields,
    $database_required_fields,
    $template_dir_name,
    $fields_table
);
$CrudCreator->createViews();
$CrudCreator->createModels();
$CrudCreator->createController();
$CrudCreator->createDatabaseRoutes();
$CrudCreator->createDatabaseNavigation();
$CrudCreator->createAclRoutes();
$CrudCreator->createDatabaseContainers();
// ##############################