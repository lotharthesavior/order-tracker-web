<!DOCTYPE html>
<html>
<head>
    <title>Resende's Crud Install</title>

    <style type="text/css">
        label{
            font-weight: bold;
        }

        code{
            display: block;
            padding: 3px 5px;
            border: 1px solid #000;
            background-color: #ccc;
            border-radius: 4px;
            margin-top: 5px;
            margin-bottom: 5px;
        }

        input{
            width: 100%;
            padding: 5px;
            border: 1px solid #000;
            border-radius: 4px;
        }

        textarea {
            width: 100%;
            min-height: 250px;
        }

        #body-container{
            max-width: 600px;
            margin: 0 auto;
        }

        .row{
            margin-bottom: 15px;
        }

        .submit{
            padding: 10px;
            max-width: 150px;
            float: right;
        }
    </style>

</head>
<body>

    <div id="body-container">

    	<form action="crud_install_post.php" method="POST">

    		<div><h1>1 Step Install</h1></div>

    		<div class="row">
    			<div><label for="database_capitalize_plural">DatabaseName-Capitalize-Plural:</label></div>
    			<div><input type="text" id="database_capitalize_plural" name="database_capitalize_plural"/></div>
    			<div>eg.: Notes</div>
    		</div>

            <div class="row">
                <div><label for="database_capitalize_singular">DatabaseName-Capitalize-Singular:</label></div>
                <div><input type="text" id="database_capitalize_singular" name="database_capitalize_singular"/></div>
                <div>eg.: Note</div>
            </div>

            <div class="row">
                <div><label for="database_lowercase_plural">DatabaseName-Lowercase-Plural:</label></div>
                <div><input type="text" id="database_lowercase_plural" name="database_lowercase_plural"/></div>
                <div>eg.: notes</div>
            </div>

            <div class="row">
                <div><label for="database_lowercase_singular">DatabaseName-Lowercase-Singular:</label></div>
                <div><input type="text" id="database_lowercase_singular" name="database_lowercase_singular"/></div>
                <div>eg.: note</div>
            </div>

            <div class="row">
                <div><label for="database_fields">Database Fields (JSON):</label></div>
                <div><textarea id="database_fields" name="database_fields"></textarea></div>
                eg.: 
                <code style="white-space: pre;">
{ 
    "title": "string", 
    "content": "string" 
}
                </code>
            </div>

            <div class="row">
                <div><label for="database_required_fields"><span style="color:red">(not implemented)</span>Database Required Fields (JSON):</label></div>
                <div><textarea id="database_required_fields" name="database_required_fields"></textarea></div>
                <div>
                    eg.:
                    <pre>
                        [];
                    </pre>
                </div>
            </div>

            <div class="row">
                <div><label for="template_dir_name">Template Directory Name:</label></div>
                <div><input type="text" id="template_dir_name" name="template_dir_name" value="sb-admin"/></div>
            </div>

            <div class="row">
                <div><label for="fields_table">Database Fields Table (JSON):</label></div>
                <div><textarea id="fields_table" name="fields_table"></textarea></div>
                <div>Obs.: this part is inconsistent, because it contains information that are used in the form and view screens.</div>
                <div>
                    eg.:
                    <code style="white-space: pre;">
{
    "Id": {
        "field_name": "id",
        "type": "input-hidden",
        "attributes": ""
    },
    "Title": {
        "field_name": "title",
        "type": "input-text",
        "attributes": "maxlength=\"60\""
    },
    "Content": {
        "list": false,
        "field_name": "content",
        "type": "textarea",
        "attributes": ""
    }
}
                    </code>
                </div>
            </div>

    		<div class="row">
                <div>
                    <input type="submit" name="action" value="Remove" class="submit" />
                    <input type="submit" name="action" value="Create" class="submit" />
                </div>
    		</div>

    	</form>

    </div>

</body>
</html>