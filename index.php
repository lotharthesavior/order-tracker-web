<?php

session_start();

//var_dump($_POST);exit;

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/vendor/phpmailer/phpmailer/PHPMailerAutoload.php';

// reference: https://www.slimframework.com/docs/objects/application.html#application-configuration

// Load configuration
$config_json = file_get_contents("config.json");
$config['settings'] = json_decode($config_json, true);

$app = new \Slim\App($config);

$container = $app->getContainer();

// Declaring Controllers -----------------------------------------------------
$container['AppController'] = function($c){
    $appcontroller = new Controllers\AppController($c);
    return $appcontroller;
};

$container['TicketsController'] = function($c){
    $ticketscontroller = new Controllers\TicketsController($c);
    return $ticketscontroller;
};

$container['UsersController'] = function($c){
    $userscontroller = new Controllers\UsersController($c);
    return $userscontroller;
};

$container['AuthController'] = function($c){
    $authcontroller = new Controllers\AuthController($c);
    return $authcontroller;
};

// Auto Generated Controllers -----------------------------------
foreach (glob(__DIR__ . '/Controllers/Generated/*-container.php') as $controller_file) {
    include $controller_file;
}
// ---------------------------------------------------------------------------

// Declaring Models -----------------------------------------------------
// $model_config = ['username'=>'MT','userpassword'=>'MT'];

$container['users'] = function($c){
    $users = new Models\Users();
    return $users;
};

$container['tickets'] = function($c){
    $tickets = new Models\Tickets();
    return $tickets;
};

// Auto Generated Models -----------------------------------
foreach (glob(__DIR__ . '/Models/Generated/*-container.php') as $model_file) {
    include $model_file;
}
foreach (glob(__DIR__ . '/Models/DB/Generated/*-container.php') as $model_db_file) {
    include $model_db_file;
}
// ---------------------------------------------------------------------

$container['view'] = new \Slim\Views\PhpRenderer($config['settings']['template-dir']);

include "acl-configuration.php";

include "routes.php";

$app->run();
