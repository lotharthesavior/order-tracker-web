<!-- Breadcrumbs -->
<ol class="breadcrumb">
	<?php foreach( $breadcrumbs as $key => $value ){ ?>
		<li class="breadcrumb-item <?php echo $value['state']; ?>">
			<?php if( !isset($value['url']) ){ ?>
				<?php echo $key; ?>
			<?php }else{ ?>
				<a href="<?php echo $value['url']; ?>"><?php echo $key; ?></a>
			<?php } ?>
		</li>
	<?php } ?>
</ol>
