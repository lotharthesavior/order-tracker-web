
    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo $template_dir ?>vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo $template_dir ?>vendor/tether/tether.min.js"></script>
    <script src="<?php echo $template_dir ?>vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo $template_dir ?>vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- <script src="<?php echo $template_dir ?>vendor/chart.js/Chart.min.js"></script> -->
    <!-- <script src="<?php echo $template_dir ?>vendor/datatables/jquery.dataTables.js"></script> -->
    <!-- <script src="<?php echo $template_dir ?>vendor/datatables/dataTables.bootstrap4.js"></script> -->

    <!-- Custom scripts for this template -->
    <!-- <script src="<?php echo $template_dir ?>js/sb-admin.min.js"></script> -->

    <script type="text/javascript" src="<?php echo $template_dir ?>js/notes-form.min.js"></script>