
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, maximum-scale=1.0, user-scalable=0">

    <meta name="description" content="Masa Ticket System">
    <meta name="author" content="Savio Resende <savio@savioresende.com.br>">
    <title>Masa - <?php echo $page; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo $template_dir ?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="<?php echo $template_dir ?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- Plugin CSS -->
    <link href="<?php echo $template_dir ?>vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo $template_dir ?>css/sb-admin.css" rel="stylesheet">

    <link href="<?php echo $template_dir ?>css/list-area.css" rel="stylesheet">

    <link href="<?php echo $template_dir ?>css/general-style.css" rel="stylesheet">

    <?php if( $page == "Orders" ){ ?>

        <!-- jquery upload -->
        <link rel="stylesheet" href="<?php echo $template_dir ?>vendor/jquery-file-upload/css/jquery.fileupload.css">

    <?php } ?>

</head>
