<div class="alert alert-<?php echo $_SESSION['message-type']; ?> alert-dismissable" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <?php echo (isset($_SESSION['message-detail']))?$_SESSION['message-detail']:$_SESSION['message']; ?>
</div>

<?php 

if( isset($_SESSION['message']) )
    unset($_SESSION['message']); 

if( isset($_SESSION['message-detail']) )
    unset($_SESSION['message-detail']); 

if( isset($_SESSION['message-type']) )
    unset($_SESSION['message-type']); 
?>
