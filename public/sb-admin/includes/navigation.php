
    <nav id="mainNav" class="navbar static-top navbar-toggleable-md navbar-inverse bg-inverse">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarExample" aria-controls="navbarExample" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <a class="navbar-brand" href="#">Resende's House DB</a>
        
        <div class="collapse navbar-collapse" id="navbarExample">
            <ul class="sidebar-nav navbar-nav">

                <?php $dashboard_active = (isset($breadcrumbs["Dashboard"]["state"]))?$breadcrumbs["Dashboard"]["state"]:""; ?>
                <li class="nav-item <?php echo $dashboard_active; ?>">
                    <a class="nav-link" href="/"><i class="fa fa-fw fa-dashboard"></i>&nbsp;Dashboard</a>
                </li>

                <?php $tickets_active = (isset($breadcrumbs["Tickets"]["state"]))?$breadcrumbs["Tickets"]["state"]:""; ?>
                <li class="nav-item <?php echo $tickets_active; ?>">
                    <a class="nav-link" href="/tickets"><i class="fa fa-comments-o" aria-hidden="true"></i>&nbsp;Tickets</a>
                </li>

                <?php $users_active = (isset($breadcrumbs["Users"]["state"]))?$breadcrumbs["Users"]["state"]:""; ?>
                <li class="nav-item <?php echo $users_active; ?>">
                    <a class="nav-link" href="/users"><i class="fa fa-comments-o" aria-hidden="true"></i>&nbsp;Users</a>
                </li>

                <?php
                // Auto Generated navigations -----------------------------------
                foreach (glob(__DIR__ . '/navigations/*-navigation.php') as $route_file) {
                    include $route_file;
                }
                // ---------------------------------------------------------
                ?>

            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/logout"><i class="fa fa-fw fa-sign-out"></i> Logout</a>
                </li>
            </ul>
        </div>
        
    </nav>
