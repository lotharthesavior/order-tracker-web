<?php $notes_active = (isset($breadcrumbs["Notes"]["state"]))?$breadcrumbs["Notes"]["state"]:""; ?>
<li class="nav-item <?php echo $notes_active; ?>">
    <a class="nav-link" href="/notes"><i class="fa fa-comments-o" aria-hidden="true"></i>&nbsp;Notes</a>
</li>