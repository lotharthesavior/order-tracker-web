
<style>
    /* TODO: move this to general-style.scss */

    .cleaner {
        clear: both;
    }

    .container-fluid {
        height: 100%;
    }

    #notes-form {
        height: 100%;
    }

    .editor-container-area {
        background: #fff;
        height: calc(100% - 112px);
        width: 100%;
        top: 120px;
        overflow: hidden;
        position: fixed;
    }

    .editor-container-area > div {
        max-height: 100% !important;
    }

    .editor-form-area {
        height: 100%;
        min-height: 100px;
        margin-bottom: 60px;
    }

    .textarea-container {
        height: calc(100% - 240px) !important;
        max-height: calc(100% - 240px) !important;
    }

    #quill_content {
        height: 100%;
        overflow: auto;
    }

</style>

<form id="notes-form" action="/notesedit" method="POST">

    <div class="row editor-container-area">

        <div class="col-lg-12">

            <div class="form-group row">

                <?php if (
                    isset($note->id)
                    && !empty($note->id)
                    && !is_null($note->id)
                ) { ?>
                    <input type="hidden" name="id" value="<?php echo $note->id; ?>">
                <?php } ?>

            </div>
            <div class="form-group row">

                <?php
                $title = "";
                if (
                    isset($note->file_content->title)
                    && !empty($note->file_content->title)
                )
                    $title = $note->file_content->title;
                ?>

                <label for="title" class="col-12 col-form-label">Title:</label>
                <div class="col-12">
                    <input
                            class="form-control"
                            type="text"
                            value="<?php echo $title; ?>"
                            name="title"
                            id="title"
                            placeholder="Title"
                            maxlength="60"
                    />
                </div>

            </div>
            <div class="form-group row textarea-container">

                <?php
                $content = "";
                if (
                    isset($note->file_content->content)
                    && !empty($note->file_content->content)
                )
                    $content = $note->file_content->content;
                ?>

                <label for="content" class="col-12 col-form-label">Content:</label>
                <div class="col-12 editor-form-area">
                    <link href="https://cdn.quilljs.com/1.3.4/quill.snow.css" rel="stylesheet">
                    <textarea
                            class="form-control"
                            name="content"
                            id="content"
                            style="display:none;"
                    ></textarea>
                    <div id="quill_content"><?php echo $content; ?></div>
                </div>

                <div class="cleaner"></div>

            </div>

        </div>

    </div>

</form>

<script src="https://cdn.quilljs.com/1.3.4/quill.js"></script>