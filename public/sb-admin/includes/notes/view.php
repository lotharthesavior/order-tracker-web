
<style>
    * {
        overflow-wrap: break-word;
    }
    
    label {
        font-weight: bold;
    }
</style>

<form id="notes-form" action="/notesedit" method="POST">

    <div class="row">

        <div class="col-lg-12">

            <div class="form-group row">

    <label for="id" class="col-12 col-form-label">Id: <?php if(
            isset($note->id)
            && !empty($note->id)
            &&!is_null($note->id)
        ){
            echo $note->id;
        } else if(
            isset($note->file_content->id)
            && !empty($note->file_content->id)
            &&!is_null($note->file_content->id)
        ){
            echo $note->file_content->id;
        } ?></label>

</div><div class="form-group row">

    <label for="title" class="col-12 col-form-label">Title:</label>
    <div class="col-12">
        <?php if(
            isset($note->title)
            && !empty($note->title)
            &&!is_null($note->title)
        ){ ?>
            <?php echo $note->title; ?>
        <?php } else if(
        isset($note->file_content->title)
            && !empty($note->file_content->title)
            &&!is_null($note->file_content->title)
        ){ ?>
            <?php echo $note->file_content->title; ?>
        <?php } ?>
    </div>

</div><div class="form-group row">

    <label for="content" class="col-12 col-form-label">Content:</label>
    <div class="col-12">
        <?php if(
            isset($note->content)
            && !empty($note->content)
            &&!is_null($note->content)
        ){ ?>
            <?php echo $note->content; ?>
        <?php } else if(
        isset($note->file_content->content)
            && !empty($note->file_content->content)
            &&!is_null($note->file_content->content)
        ){ ?>
            <?php echo $note->file_content->content; ?>
        <?php } ?>
    </div>

</div>

        </div>

    </div>

</form>
