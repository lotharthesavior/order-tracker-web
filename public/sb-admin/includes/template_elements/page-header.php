<?php /* header */ ?>

    <h1 class="page-header">
        <?php echo $title; ?>
        <small>&nbsp;</small>

        <?php echo (isset($extra_content)) ? $extra_content : ""; ?>
    </h1>
    
<?php /* / header */ ?>
