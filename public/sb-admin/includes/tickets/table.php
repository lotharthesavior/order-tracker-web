<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Subject</th>
                        <th>Content</th>
                        <th>Options</th>
                    </tr>
                </thead>
                <tbody>
                    <?php

                    if (!empty($tickets))
                    foreach ($tickets->results as $key => $value) { 

                        // TODO: temp solution
                        $value = (array) $value;
                        $value['file_content'] = (array) $value['file_content'];

                        ?>
                        
                        <tr class="table-active">
                            <td><?php echo $value['id']; ?></td>
                            <td><a href="/ticketsedit/<?php echo $value['id']; ?>"><?php echo $value['file_content']['title']; ?></a></td>
                            <td>
                            <?php 
                                echo substr($value['file_content']['description'], 0, 25) 
                                     . ((strlen($value['file_content']['description']) > 25)?'...':''); 
                            ?></td>
                            <td>
                                <a 
                                    onclick="if(!confirm('Do you really want to delete this record?')){return false}" 
                                    href="/ticket-delete/<?php echo $value['id']; ?>"
                                >
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>

                    <?php } ?>

                </tbody>
            </table>
        </div>
    </div>
</div>
