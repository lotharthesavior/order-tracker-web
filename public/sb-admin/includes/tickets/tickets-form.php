
<form id="tickets-form" action="/ticketsedit" method="POST">

<div class="row">

    <div class="col-lg-12">

        <?php if( 
            isset($ticket->id) 
            && !empty($ticket->id)
            &&!is_null($ticket->id)
        ){ ?>
            <input type="hidden" name="id" value="<?php echo $ticket->id; ?>">
        <?php } ?>

        <div class="form-group row">

            <!-- <div class="col-2">&nbsp;</div>
            <div class="col-10">
               <a class="btn btn-primary" name="submit" onclick="javascript:$('#tickets-form').submit()" href="#"><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Save</a>
            </div> -->

        </div>

        <div class="form-group row">

            <?php
            $title = "";
            if( 
                isset($ticket->file_content->title)
                && !empty($ticket->file_content->title)
            )
                $title = $ticket->file_content->title;
            ?>
        
            <label for="title" class="col-2 col-form-label">Brief Description:</label>
            <div class="col-10">
                <input 
                    class="form-control" 
                    type="text" 
                    value="<?php echo $title; ?>" 
                    name="title"
                    id="title"
                    placeholder="Brief Description..."
                    maxlength="50"
                />
            </div>

        </div>

        <div class="form-group row">

            <?php
            $description = "";
            if( 
                isset($ticket->file_content->description)
                && !empty($ticket->file_content->description)
            )
                $description = $ticket->file_content->description;
            ?>

            <label for="description" class="col-2 col-form-label">Information:</label>
            <div class="col-10">
                <textarea class="form-control" id="description" rows="5" name="description"><?php echo $description; ?></textarea>
            </div>

        </div>

        <div class="form-group row">

            <?php
            $status = "";
            if( 
                isset($ticket->file_content->status)
                && !empty($ticket->file_content->status)
            )
                $status = $ticket->file_content->status;

            $status_options = [
                'Open' => 'open',
                'Processing' => 'processing',
                'Closed' => 'closed'
            ];
            ?>

            <label for="status" class="col-2 col-form-label">Status:</label>
            <div class="col-10">
                <select class="form-control" id="status" name="status">
                    <?php foreach ($status_options as $key => $value) { ?>
                        <option 
                            value="<?php echo $value; ?>"
                            <?php ($status == $value)?"selected":""; ?>
                        ><?php echo $key; ?></option>
                    <?php } ?>
                </select>
            </div>

        </div>

        <div class="form-group row">

            <?php
            // $description = "";
            // if( 
            //     isset($ticket->file_content->description)
            //     && !empty($ticket->file_content->description)
            // )
            //     $description = $ticket->file_content->description;
            ?>

            <label for="type" class="col-2 col-form-label">Type:</label>
            <div class="col-10">
                <select name="type" class="form-control" id="type">
                    <option value="order">Order</option>
                </select>
            </div>

        </div>

        <!-- file upload -->
            <div class="form-group row">

                <?php
                // var_dump($ticket->file_content);exit;
                // $description = "";
                // if( 
                //     isset($ticket->file_content->description)
                //     && !empty($ticket->file_content->description)
                // )
                //     $description = $ticket->file_content->description;
                ?>
                
                <label for="type" class="col-2 col-form-label">Attach Files:</label>
                <div class="col-10">
                    <!-- The fileinput-button span is used to style the file input field as button -->
                    <span class="btn btn-success fileinput-button">
                        <i class="glyphicon glyphicon-plus"></i>
                        <span>Select files...</span>
                        <!-- The file input field used as target for the file upload widget -->
                        <input id="fileupload" type="file" name="files[]" multiple>
                    </span>
                    <br>
                    <br>
                    <!-- The global progress bar -->
                    <div id="progress" class="progress">
                        <div class="progress-bar progress-bar-success"></div>
                    </div>
                    <!-- The container for the uploaded files -->
                    <div id="files" class="files">
                        <?php
                        if(
                            isset($ticket->file_content)
                            && isset($ticket->file_content->files)
                            && !empty($ticket->file_content->files)
                        )

                        // var_dump($ticket->file_content->files);exit;
                        foreach ($ticket->file_content->files as $key => $file) {

                            $file_unique_id = uniqid('files_');

                            $file_location = "/uploads/". $file->file_name;
                            if( !file_exists("/uploads/". $file->file_name) ){
                                $file_info = explode(".", $file->file_name);
                                $file_location = "/uploads-db/tickets/" . $ticket->id . "/" . $file_info[0] . "/" . $file_info[1];
                            }
                            // $file_location = "/uploads/tickets/" . $ticket->id . "/" . $file->file_name . "/{extension}";
                            
                            ?>
                            
                            <input
                                type="hidden"
                                name="file_name[]"
                                value="<?php echo $file->file_name; ?>"
                                class="<?php echo $file_unique_id; ?>"
                            />

                            <input
                                type="hidden",
                                name="file_original_name[]",
                                value="<?php echo $file->file_original_name; ?>"
                                class="<?php echo $file_unique_id; ?>"
                            />

                            <p
                                class="<?php echo $file_unique_id; ?>"
                                style="display: inline;"
                            ><a target="_blank" href="<?php echo $file_location; ?>"><?php echo $file->file_original_name; ?></a></p>

                            <a 
                                onclick="$('.<?php echo $file_unique_id; ?>').remove()"
                                class="<?php echo $file_unique_id; ?>"
                                style="display: inline;
                                       margin-left:15px;
                                       line-height:30px;
                                       cursor: pointer;"
                            >X</a>

                            <br class="<?php echo $file_unique_id; ?>"/>

                        <?php } ?>
                    </div>
                </div>
                
            </div>
        <!-- / file upload -->

	    <div class="form-group row">

            <div class="col-2">&nbsp;</div>
            <div class="col-10">
               <a class="btn btn-primary" name="submit" onclick="javascript:$('#tickets-form').submit()" href="#"><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Save</a>
            </div>

        </div>

    </div>

</div>

</form>
