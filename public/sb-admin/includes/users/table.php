<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Options</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($users))
                    foreach ($users->results as $key => $value) {

                        // TODO: temp solution
                        $value = (array) $value;
                        $value['file_content'] = (array) $value['file_content'];

                        ?>
                        
                        <tr class="table-active">
                            <td><?php echo $value['id']; ?></td>
                            <td><a href="/usersedit/<?php echo $value['id']; ?>"><?php echo $value['file_content']['name']; ?></a></td>
                            <td>
                                <a href="/user/<?php echo $value['id']; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <a
                                        onclick="if(!confirm('Do you really want to delete this record?')){return false}"
                                        href="/user-delete/<?php echo $value['id']; ?>"
                                ><i class="fa fa-trash" aria-hidden="true"></i></a>
                            </td>
                        </tr>

                    <?php } ?>

                </tbody>
            </table>
        </div>
    </div>
</div>
