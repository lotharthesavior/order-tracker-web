
<form id="users-form" action="/usersedit" method="POST">

<div class="row">

    <div class="col-lg-12">

        <div class="form-group row">

            <?php if(
                isset($user->id)
                && !empty($user->id)
                &&!is_null($user->id)
            ){ ?>
                <input type="hidden" name="id" value="<?php echo $user->id; ?>">
            <?php } ?>

        </div>

        <div class="form-group row">

            <?php
            $name = "";
            if( 
                isset($user->file_content->name)
                && !empty($user->file_content->name)
            )
                $name = $user->file_content->name;
            ?>
        
            <label for="name" class="col-2 col-form-label">Name:</label>
            <div class="col-10">
                <input 
                    class="form-control" 
                    type="name" 
                    value="<?php echo $name; ?>" 
                    name="name"
                    id="name"
                    placeholder="Name"
                    maxlength="50"
                />
            </div>

        </div>

        <div class="form-group row">

            <?php
            $email = "";
            if( 
                isset($user->file_content->email)
                && !empty($user->file_content->email)
            )
                $email = $user->file_content->email;
            ?>
        
            <label for="email" class="col-2 col-form-label">Email:</label>
            <div class="col-10">
                <input 
                    class="form-control" 
                    type="email" 
                    value="<?php echo $email; ?>" 
                    name="email"
                    id="email"
                    placeholder="Name"
                    maxlength="50"
                />
            </div>

        </div>

        <div class="form-group row">

            <label for="password" class="col-2 col-form-label">Password:</label>
            <div class="col-10">
                <input 
                    class="form-control" 
                    type="password" 
                    value="" 
                    name="password"
                    id="password"
                    placeholder="Password"
                    maxlength="50"
                />
            </div>

        </div>

        <div class="form-group row">

            <label for="password_confirmation" class="col-2 col-form-label">Password Confirmation:</label>
            <div class="col-10">
                <input 
                    class="form-control" 
                    type="password"
                    value="" 
                    name="password_confirmation"
                    id="password_confirmation"
                    placeholder="Password Confirmation"
                    maxlength="50"
                />
            </div>

        </div>

        <div class="form-group row">

            <?php
            $user_type = "";
            if( 
                isset($user->file_content->user_type)
                && !empty($user->file_content->user_type)
            ) {
                $user_type = $user->file_content->user_type;
            }

            $user_type_options = [
                'Administrator' => 'administrator'
            ];
            ?>

            <label for="user_type" class="col-2 col-form-label">User Type:</label>
            <div class="col-10">
                <select class="form-control" id="user_type" name="user_type">
                    <option></option>
                    <?php foreach ($user_type_options as $key => $value) { ?>
                        <option 
                            value="<?php echo $value; ?>"
                            <?php echo ($user_type === $value) ? "selected" : "" ; ?>
                        ><?php echo $key; ?></option>
                    <?php } ?>
                </select>
            </div>

        </div>

	    <div class="form-group row">

            <div class="col-2">&nbsp;</div>
            <div class="col-10">
               <a class="btn btn-primary" name="submit" onclick="javascript:$('#users-form').submit()" href="#"><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Save</a>
            </div>

        </div>

    </div>

</div>

</form>
