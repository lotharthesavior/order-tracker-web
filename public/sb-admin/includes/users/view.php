
<form id="users-form" action="/usersedit" method="POST">

    <div class="row">

        <div class="col-lg-12">

            <div class="form-group row">

                <label for="name" class="col-2 col-form-label">Name:</label>
                <div class="col-10">
                    <?php if(
                        isset($user->id)
                        && !empty($user->id)
                        &&!is_null($user->id)
                    ){ ?>
                        <?php echo $user->id; ?>
                    <?php } ?>
                </div>

            </div>

            <div class="form-group row">

                <?php
                $name = "";
                if(
                    isset($user->file_content->name)
                    && !empty($user->file_content->name)
                )
                    $name = $user->file_content->name;
                ?>

                <label for="name" class="col-2 col-form-label">Name:</label>
                <div class="col-10">
                    <?php echo $name; ?>
                </div>

            </div>

            <div class="form-group row">

                <?php
                $email = "";
                if(
                    isset($user->file_content->email)
                    && !empty($user->file_content->email)
                )
                    $email = $user->file_content->email;
                ?>

                <label for="email" class="col-2 col-form-label">Email:</label>
                <div class="col-10">
                    <?php echo $email; ?>
                </div>

            </div>

            <div class="form-group row">

                <?php
                $user_type = "";
                if(
                    isset($user->file_content->user_type)
                    && !empty($user->file_content->user_type)
                ) {
                    $user_type = $user->file_content->user_type;
                }

                $user_type_options = [
                    'Administrator' => 'administrator'
                ];
                ?>

                <label for="user_type" class="col-2 col-form-label">User Type:</label>
                <div class="col-10">
                    <?php echo $user_type; ?>
                </div>

            </div>

        </div>

    </div>

</form>
