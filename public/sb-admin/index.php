<!DOCTYPE html>
<html lang="en">

<?php

include "includes/header.php";

?>

<body id="page-top">

    <?php 

    include "includes/navigation.php"; 

    ?>

    <div class="content-wrapper py-3">

        <div class="container-fluid">

	    <?php 

        include "includes/breadcrumbs.php";

        include "includes/template_elements/page-header.php";
        
        ?> 

	    <div class="row">
                    <div class="col-lg-12">
<!--                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <i class="fa fa-info-circle"></i>  <strong>Welcome to Masa Order Tracker!</strong> Here you have access and Control of all of your information on Masa Inc.
                        </div> -->
                    </div>
            </div>

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /.content-wrapper -->

    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fa fa-chevron-up"></i>
    </a>

    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo $template_dir ?>vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo $template_dir ?>vendor/tether/tether.min.js"></script>
    <script src="<?php echo $template_dir ?>vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo $template_dir ?>vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="<?php echo $template_dir ?>js/sb-admin.min.js"></script>

</body>

</html>
