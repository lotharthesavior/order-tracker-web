
/**
 * Submit Form with Quill Content
 */
function quillBeforeSubmit() {
    var quillContents = $('#quill_content').find('.ql-editor').html();
    $('#content').val(quillContents);
    $('#notes-form').submit();
}

$(document).ready(function(){
    var quill = new Quill('#quill_content', {
        theme: 'snow'
    });

    // clear the window for the editor
    $('.content-wrapper').css({marginLeft: '0px', background: '#fff !important'});
    $('nav').remove();
});