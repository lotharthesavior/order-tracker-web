<!DOCTYPE html>
<html lang="en">

<?php 
$page = "Login";
include "includes/header.php";
?>

<link href="<?php echo $template_dir ?>css/auth-style.css" rel="stylesheet">

<body id="page-top">

    <div 
        class="container login-form-container"
    >
            <div><center><h3>Masa Order Tracker - Login</h3></center></div>

            <?php
            if( isset($_SESSION['message']) ){  
                include "includes/messages-area.php"; 
            }   
            ?>

            <br/>

            <form action="/login"  method="POST" id="login-form">

            <div class="form-group row">
                <label for="email">Email address</label>
                <input name="email" type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email">
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
 
            <div class="form-group row">
                <label for="password">Password</label>
                <input name="password" type="password" class="form-control" id="password" placeholder="Password">
            </div>

            <br/>

            <div class="form-group row">
                <a class="btn btn-primary login-input-submit" onclick="javascript:$('#login-form').submit();" href="#">Login</a>
            </div>

            <div class="form-group row small-links">
                <div class="left-link col-6">
                    <a href="/register" class="">Register</a>
                </div>
                <div class="right-link col-6">
                    <a href="/forget-password" class="">Forget Password?</a>
                </div>
            </div>

            </form>

    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo $template_dir ?>vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo $template_dir ?>vendor/tether/tether.min.js"></script>
    <script src="<?php echo $template_dir ?>vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo $template_dir ?>vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="<?php echo $template_dir ?>vendor/chart.js/Chart.min.js"></script>
    <script src="<?php echo $template_dir ?>vendor/datatables/jquery.dataTables.js"></script>
    <script src="<?php echo $template_dir ?>vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for this template -->
    <script src="<?php echo $template_dir ?>js/sb-admin.min.js"></script>

</body>

</html>
