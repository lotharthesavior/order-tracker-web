<!DOCTYPE html>
<html lang="en">

<?php 
$page = "Login";
include "includes/header.php";
?>

<link href="<?php echo $template_dir ?>css/auth-style.css" rel="stylesheet" />

<body id="page-top">

    <div 
        class="container login-form-container"
    >
            <div><center><h3>Masa Order Tracker - New Password</h3></center></div>

            <br/>

            <form action="/newPasswordPost" method="POST" id="new-password-form">

            <div class="form-group row">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password">
            </div>

            <div class="form-group row">
                <label for="exampleInputPassword1">Confirm Password</label>
                <input type="confirm-password" class="form-control" id="exampleInputPassword1" placeholder="Confirm Password" name="confirm-password" />
            </div> 

            <br/>

            <div class="form-group row">
                <a class="btn btn-primary login-input-submit" onclick="javascript:$('#new-password-form').submit();" href="#">Change Password</a>
            </div>

            </form>

    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo $template_dir ?>vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo $template_dir ?>vendor/tether/tether.min.js"></script>
    <script src="<?php echo $template_dir ?>vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo $template_dir ?>vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="<?php echo $template_dir ?>vendor/chart.js/Chart.min.js"></script>
    <script src="<?php echo $template_dir ?>vendor/datatables/jquery.dataTables.js"></script>
    <script src="<?php echo $template_dir ?>vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for this template -->
    <script src="<?php echo $template_dir ?>js/sb-admin.min.js"></script>

</body>

</html>
