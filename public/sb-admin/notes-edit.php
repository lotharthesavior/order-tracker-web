<!DOCTYPE html>
<html lang="en">

<?php 

$page = "Notes";
include "includes/header.php"; 

?>

<body id="page-top">

    <?php include "includes/navigation.php"; ?>

    <div class="content-wrapper py-3">

        <div class="container-fluid">

	    <?php include "includes/breadcrumbs.php"; ?>

	    <?php

	    $title = "New Note";
        if (isset($note->id)) {
            $title = "Edit Note";
        }

        $extra_content = '<a style="margin-top:-14px;" class="btn btn-primary" name="submit" onclick="javascript:quillBeforeSubmit();" href="#"><i
                                class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Save</a>';

	    include "includes/template_elements/page-header.php";

	    ?>

	    <br/>

	    <?php include "includes/notes/notes-form.php"; ?>

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /.content-wrapper -->

    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fa fa-chevron-up"></i>
    </a>

    <?php include "includes/footer.php"; ?>


    <!-- jquery upload -->
        <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
        <script src="<?php echo $template_dir ?>vendor/jquery-file-upload/js/vendor/jquery.ui.widget.js"></script>
        <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
        <script src="<?php echo $template_dir ?>vendor/jquery-file-upload/js/jquery.iframe-transport.js"></script>
        <!-- The basic File Upload plugin -->
        <script src="<?php echo $template_dir ?>vendor/jquery-file-upload/js/jquery.fileupload.js"></script>

        <script>
        var files_in_form = 0;

        <?php  ?>

        function informUserUpload(msg){
            var tag_p = $("<p>")
                            .css({
                                display: "inline",
                                marginLeft: "15px",
                                color: "red"
                            })
                            .html(msg);
            $("#fileupload").parent().after(tag_p);
            setTimeout(function(){
                tag_p.hide().remove();
            }, 5000);
        }

        /*jslint unparam: true */
        /*global window, $ */
        $(function () {
            'use strict';
            // Change this to the location of your server-side upload handler:
            var url = '/file-upload';
            $('#fileupload').fileupload({
                url: url,
                dataType: 'json',
                done: function (e, data) {
                    $.each(data.result.files, function (index, file) {
                        <?php $file_unique_id = uniqid('files_'); ?>

                        var file_name = $('<input />').attr({
                            type: 'hidden',
                            name: 'file_name[]',
                            value: file.name,
                            class: "<?php echo $file_unique_id; ?>"
                        });

                        var file_original_name = $('<input />').attr({
                            type: 'hidden',
                            name: 'file_original_name[]',
                            value: file.original_name,
                            class: "<?php echo $file_unique_id; ?>"
                        });

                        var tag_a = $("<a>")
                                        .attr("target", "_blank")
                                        .attr("href", "/uploads/" + file.name)
                                        .attr("class", "<?php echo $file_unique_id; ?>");

                        var remove_link = $("<a>")
                                                .attr("class", "<?php echo $file_unique_id; ?>")
                                                .css({
                                                    display: "inline",
                                                    marginLeft: "15px",
                                                    lineHeight: "30px",
                                                    cursor: "pointer"
                                                })
                                                .on("click", function(){
                                                    $(".<?php echo $file_unique_id; ?>").remove();
                                                })
                                                .html("X");

                        var tag_p = $('<p/>')
                            .text(file.original_name)
                            .css("display", "inline")
                            .attr("class", "<?php echo $file_unique_id; ?>")
                            .append(file_name)
                            .append(file_original_name)
                            .appendTo(tag_a);

                        tag_a
                            .appendTo('#files')
                            .after($("<br/>").attr("class", "<?php echo $file_unique_id; ?>"))
                            .after(remove_link);

                        informUserUpload("You file is not saved yet, save this Form for that.");
                    });
                },
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress .progress-bar').css(
                        'width',
                        progress + '%'
                    );
                }
            }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
        });
        </script>
    <!-- / jquery upload -->

</body>

</html>
