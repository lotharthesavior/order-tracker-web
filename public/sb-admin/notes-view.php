<!DOCTYPE html>
<html lang="en">

<?php 

$page = "Notes";
include "includes/header.php"; 

?>

<body id="page-top">

    <?php include "includes/navigation.php"; ?>

    <div class="content-wrapper py-3">

        <div class="container-fluid">

	    <?php include "includes/breadcrumbs.php"; ?>

	    <?php
        
	    $title = "Note View";
	    include "includes/template_elements/page-header.php";

	    ?>

	    <br/>

	    <?php include "includes/notes/view.php"; ?>

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /.content-wrapper -->

    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fa fa-chevron-up"></i>
    </a>

    <?php include "includes/footer.php"; ?>

    <!-- jquery upload -->
        <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
        <script src="<?php echo $template_dir ?>vendor/jquery-file-upload/js/vendor/jquery.ui.widget.js"></script>
        <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
        <script src="<?php echo $template_dir ?>vendor/jquery-file-upload/js/jquery.iframe-transport.js"></script>
        <!-- The basic File Upload plugin -->
        <script src="<?php echo $template_dir ?>vendor/jquery-file-upload/js/jquery.fileupload.js"></script>
    <!-- / jquery upload -->

</body>

</html>
