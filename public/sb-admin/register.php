<!DOCTYPE html>
<html lang="en">

<?php 
$page = "Login";
include "includes/header.php";
?>

<link href="<?php echo $template_dir ?>css/auth-style.css" rel="stylesheet" />

<body id="page-top">

    <div 
        class="container login-form-container"
    >
            <div><center><h3>Masa Order Tracker - Register</h3></center></div>

            <?php
            if( isset($_SESSION['message']) ){ 
                include "includes/messages-area.php"; 
            }
            ?>

            <br/>

            <form action="/register" method="POST" id="register-form">

            <?php
            $has_error = "";
            if(
                (
                    isset($_SESSION["missing_fields"]) 
                    && in_array("email", $_SESSION["missing_fields"])
                ) OR (
                    isset($_SESSION["wrong_type_fields"])
                    && in_array("email", $_SESSION["wrong_type_fields"])
                )
            ){
                $has_error = "has-danger";
            }
            ?>

            <div class="form-group row <?php echo $has_error; ?>">
                <label for="email">Email address</label>
                <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email" />
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>

            <?php
            $has_error = ""; 
            if( 
                (   
                    isset($_SESSION["missing_fields"]) 
                    && in_array("password", $_SESSION["missing_fields"])
                ) OR (
                    isset($_SESSION["wrong_type_fields"])
                    && in_array("password", $_SESSION["wrong_type_fields"])
                )   
            ){  
                $has_error = "has-danger";
            }   
            ?> 

            <div class="form-group row <?php echo $has_error; ?>">
                <label for="password">Password</label>
                <input type="password" name="password" class="form-control" id="password" placeholder="Password" />
            </div>

            <br/>

            <div class="form-group row">
                <a class="btn btn-primary login-input-submit" onclick="javascript:$('#register-form').submit();" href="#">Register</a>
            </div>

            <div class="form-group row small-links">
                <div class="left-link col-6">
                    <a href="/login" class="">Login</a>
                </div>  
                <div class="right-link col-6">
                    <a href="/forget-password" class="">Forget Password</a>
                </div>  
            </div>

            </form>

    </div>

    <?php 

    if( isset($_SESSION['missing_fields']) )
        unset($_SESSION['missing_fields']);

    if( isset($_SESSION['wrong_type_fields']) )
        unset($_SESSION['wrong_type_fields']);

    include "includes/footer.php"; 
    
    ?>

</body>

</html>
