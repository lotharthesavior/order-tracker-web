<?php

// these are the interfaces of Request and Respons. This is the Web guys =)
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;


// Home ----------------------------------------------------------------

$app->get('/', 'AppController:home');

$app->get('/example', 'AppController:example');

// / Home --------------------------------------------------------------


// Tickets ----------------------------------------------------------------

$app->get('/tickets', 'TicketsController:home');

$app->get('/ticket/{id}', 'TicketsController:view');

$app->get('/ticket-delete/{id}', 'TicketsController:remove');

$app->get('/ticketsedit', 'TicketsController:create');

$app->get('/ticketsedit/{id}', 'TicketsController:edit');

$app->get('/tickets-comments/{ticket}', 'TicketsController:comments');

/**
 * @todo Improve messaging system to return result of operation to user
 */
$app->post('/ticketsedit', 'TicketsController:editPost');

$app->post('/tickets-comment-edit', 'TicketsController:commmentPost');

$app->delete('/tickets-comment-delete/{id}', 'TicketsController:deletePostCommment');

$app->post('/tracker-test-edit', 'TicketsController:trackerTestPost');

$app->post('/tracker-fix-edit', 'TicketsController:trackerFixPost');

$app->delete('/tracker-fix-delete/{id}', 'FixController:deleteFix');

$app->post('/tickets-responsible-edit', 'TicketsController:responsibleEdit');

$app->post('/removeOccurrence', 'TicketsController:removeOccurrence');

// / Tickets -------------------------------------------------------------


// Users ----------------------------------------------------------------

$app->get('/users', 'UsersController:home');

$app->get('/user/{id}', 'UsersController:view');

$app->get('/user-delete/{id}', 'UsersController:remove');

$app->get('/usersedit', 'UsersController:create');

$app->get('/usersedit/{id}', 'UsersController:edit');

$app->post('/usersedit', 'UsersController:editPost');

// / Users -------------------------------------------------------------


// Auth -----------------------------------------------------------

$app->get('/login', 'AuthController:loginForm');

$app->post('/login', 'AuthController:loginPost');

$app->get('/logout', 'AuthController:logout');

$app->get('/register', 'AuthController:registerForm');

$app->post('/register', 'AuthController:registerPost');

$app->get('/not-authorized', 'AppController:notAuthorized');

$app->get('/forget-password', 'AuthController:forgetPasswordForm');

$app->post('/forget-password', 'AuthController:forgetPasswordPost');

$app->get('/new-password', 'AuthController:newPasswordForm');

$app->post('/new-password', 'AuthController:newPasswordPost');

// / Auth ---------------------------------------------------------


// Generic ---------------------------------------------------------

$app->post('/file-upload', 'AppController:uploadFile');

$app->get('/uploads-db/{database}/{record}/{file}/{extension}', 'AppController:downloadFile');

// / Generic ---------------------------------------------------------


// Auto Generated routes -----------------------------------
foreach (glob("routes/*-routes.php") as $route_file) {
    include $route_file;
}
// ---------------------------------------------------------