<?php

$permissions_list['resources'][] = "/notes";
$permissions_list['resources'][] = '/note/{id}';
$permissions_list['resources'][] = "/notesedit";
$permissions_list['resources'][] = '/notesedit/{id}';
$permissions_list['resources'][] = '/note-delete/{id}';

$permissions_list["routes"] = ['/notesedit', ['administrator'], ['get', 'post']];

$permissions_list["assignments"]["allow"]["administrator"][] = "/notes";
$permissions_list["assignments"]["allow"]["administrator"][] = '/note/{id}';;
$permissions_list["assignments"]["allow"]["administrator"][] = "/notesedit";
$permissions_list["assignments"]["allow"]["administrator"][] = '/notesedit/{id}';
$permissions_list["assignments"]["allow"]["administrator"][] = '/note-delete/{id}';