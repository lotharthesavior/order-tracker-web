<?php

// Notes ----------------------------------------------------------------

$app->get('/notes', 'NotesController:home');

$app->get('/note/{id}', 'NotesController:view');

$app->get('/note-delete/{id}', 'NotesController:remove');

$app->get('/notesedit', 'NotesController:create');

$app->get('/notesedit/{id}', 'NotesController:edit');

$app->post('/notesedit', 'NotesController:editPost');

// / Notes -------------------------------------------------------------