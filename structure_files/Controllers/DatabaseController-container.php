<?php

$container['{{database-capitalize-plural}}Controller'] = function($c){
    ${{database-capitalize-singular}}controller = new Controllers\Generated\{{database-capitalize-plural}}Controller($c);
    return ${{database-capitalize-singular}}controller;
};