<?php

$container['{{database-lowercase-singular}}'] = function($c){
    ${{database-lowercase-singular}} = new Models\DB\Generated\{{database-capitalize-singular}}();
    return ${{database-lowercase-singular}};
};