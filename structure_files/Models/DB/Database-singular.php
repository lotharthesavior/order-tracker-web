<?php

/**
 * {{database-capitalize-plural}} Model
 *
 * This is a Model to keep the business rules.
 */

namespace Models\Generated\DB;

class {{database-capitalize-singular}} {

    {{protected-model-database-fields}}

    public function __set($name, $value){
        $this->{$name} = $value;
    }

    public function __get($name){
        return $this->{$name};
    }

}
