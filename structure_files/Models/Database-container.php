<?php

$container['{{database-lowercase-plural}}'] = function($c){
    ${{database-lowercase-plural}} = new Models\Generated\{{database-capitalize-plural}}();
    return ${{database-lowercase-plural}};
};