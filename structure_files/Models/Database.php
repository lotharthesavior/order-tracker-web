<?php

/**
 * {{database-capitalize-plural}} Model
 *
 * This is a Model to keep the business rules.
 *
 * @author Savio Resende <savio@minisisinc.com>
 */

namespace Models\Generated;

use \Marina\Marina;

class {{database-capitalize-plural}} extends Marina {

	protected $database = "{{database-lowercase-plural}}";

    protected $fields = [
        {{database-fields}}
    ];

	protected $required_fields = [
        {{database-required-fields}}
	];

    protected $constraints = [ 
        {{database-constraints}}
    ];

	/**
	 * Constructor
     *
	 * @return $this instance
	 */
	public function __construct() {
	    $config_param['database'] = $this->database;

	    parent::__construct($config_param);
	}

    /**
     * Fill the Single Model
     * 
     * @param array $data
     * @return {{database-capitalize-singular}}
     */
    protected function fillModel(array $data){
        ${{database-lowercase-singular}}_instance = new {{database-capitalize-singular}};

        foreach($data as $key => $value){
            ${{database-lowercase-singular}}_instance->{$key} = $value;
        }

        return ${{database-lowercase-singular}}_instance;
    }

	/**
	 * Find Page
	 *
	 * @param int $sisn
	 */
	public function find($id) {
        $result = $this->search(["id" => $id]);

        return $result;
	}

	/**
	 * 
	 */
	public function findAll(){
		$result = $this->search([]);
		
		return $result;
	}

    /**
     * @internal {search clusters section} is the part where we can search multiple fields using only one expression
     *
     * @param array $params
     */
    public function search(array $params, $recall = false)
    {
        // search clusters section
        if( isset($params["search"]) && !empty($params["search"]) ){
            foreach ($this->fields as $field) {
                $params[$field] = $params['search'];
            }
            $params['logic'] = 1;
            unset($params["search"]);
        }

        $results = parent::searchRecords($params, $recall);

        $results->results = $this->_filterSchema($results->results);

        return $results;
    }

    /**
     *
     */
    public function save(array $client_data)
    {
        return parent::save($client_data);
    }

}
