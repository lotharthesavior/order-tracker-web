<?php

$permissions_list['resources'][] = "/{{database-lowercase-plural}}";
$permissions_list['resources'][] = '/{{database-lowercase-singular}}/{id}';
$permissions_list['resources'][] = "/{{database-lowercase-plural}}edit";
$permissions_list['resources'][] = '/{{database-lowercase-plural}}edit/{id}';
$permissions_list['resources'][] = '/{{database-lowercase-singular}}-delete/{id}';

$permissions_list["routes"] = ['/{{database-lowercase-plural}}edit', ['administrator'], ['get', 'post']];

$permissions_list["assignments"]["allow"]["administrator"][] = "/{{database-lowercase-plural}}";
$permissions_list["assignments"]["allow"]["administrator"][] = '/{{database-lowercase-singular}}/{id}';;
$permissions_list["assignments"]["allow"]["administrator"][] = "/{{database-lowercase-plural}}edit";
$permissions_list["assignments"]["allow"]["administrator"][] = '/{{database-lowercase-plural}}edit/{id}';
$permissions_list["assignments"]["allow"]["administrator"][] = '/{{database-lowercase-singular}}-delete/{id}';