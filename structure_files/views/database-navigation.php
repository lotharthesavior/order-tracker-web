<?php ${{database-lowercase-plural}}_active = (isset($breadcrumbs["{{database-capitalize-plural}}"]["state"]))?$breadcrumbs["{{database-capitalize-plural}}"]["state"]:""; ?>
<li class="nav-item <?php echo ${{database-lowercase-plural}}_active; ?>">
    <a class="nav-link" href="/{{database-lowercase-plural}}"><i class="fa fa-comments-o" aria-hidden="true"></i>&nbsp;{{database-capitalize-plural}}</a>
</li>