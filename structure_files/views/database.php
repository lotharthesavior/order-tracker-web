<!DOCTYPE html>
<html lang="en">

<?php 
$page = "{{database-capitalize-plural}}";
include "includes/header.php"; 
?>

<body id="page-top">

    <?php include "includes/navigation.php"; ?>

    <div class="content-wrapper py-3">

        <div class="container-fluid">

	    <?php include "includes/breadcrumbs.php"; ?>

	    <?php
	    $title = "{{database-capitalize-plural}}";
	    include "includes/template_elements/page-header.php";
	    ?>

	    <br/>

	    <div class="row">
            <div class="col-lg-6">
                <a class="btn btn-primary" href="/{{database-lowercase-plural}}edit"><i class="fa fa-plus" aria-hidden="true"></i>
&nbsp;New {{database-capitalize-singular}}</a>
            </div>

	        <div class="col-lg-6">
                <form action="/{{database-lowercase-plural}}" method="GET">
                    <div class="input-group">
                        <input type="text" name="search" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="button">Go!</button>
                        </span>
                    </div><!-- /input-group -->
                </form>
            </div>
	    </div>

        <br/>

        <?php if( isset($_SESSION['message']) && !empty($_SESSION['message']) ){ 

            $message_type = "alert alert-danger";
            if( isset($_SESSION['message-positive']) ){
                $message_type = "alert alert-success";
            } ?>

            <div class="<?php echo $message_type; ?>"><?php echo $_SESSION['message']; ?></div>

        <?php } ?>

	    <br/>

	    <?php include "includes/{{database-lowercase-plural}}/table.php"; ?>

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /.content-wrapper -->

    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fa fa-chevron-up"></i>
    </a>

    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo $template_dir ?>vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo $template_dir ?>vendor/tether/tether.min.js"></script>
    <script src="<?php echo $template_dir ?>vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo $template_dir ?>vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="<?php echo $template_dir ?>vendor/chart.js/Chart.min.js"></script>
    <script src="<?php echo $template_dir ?>vendor/datatables/jquery.dataTables.js"></script>
    <script src="<?php echo $template_dir ?>vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for this template -->
    <script src="<?php echo $template_dir ?>js/sb-admin.min.js"></script>

</body>

</html>
