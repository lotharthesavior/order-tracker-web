<div class="form-group row">

    <?php
    ${{field-id}} = "";
    if(
        isset(${{database-lowercase-singular}}->file_content->{{field-id}})
        && !empty(${{database-lowercase-singular}}->file_content->{{field-id}})
    )
        ${{field-id}} = ${{database-lowercase-singular}}->file_content->{{field-id}};
    ?>

    <label for="{{field-id}}" class="col-2 col-form-label">{{field-label}}:</label>
    <div class="col-10">
        <input
            class="form-control"
            type="text"
            value="<?php echo ${{field-id}}; ?>"
            name="{{field-id}}"
            id="{{field-id}}"
            placeholder="{{field-label}}"
            {{field-input-attributes}}
        />
    </div>

</div>