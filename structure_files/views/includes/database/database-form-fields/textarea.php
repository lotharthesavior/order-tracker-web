<div class="form-group row">

    <?php
    ${{field-id}} = "";
    if(
    isset(${{database-lowercase-singular}}->file_content->{{field-id}})
                && !empty(${{database-lowercase-singular}}->file_content->{{field-id}})
            )
                ${{field-id}} = ${{database-lowercase-singular}}->file_content->{{field-id}};
            ?>

    <label for="{{field-id}}" class="col-2 col-form-label">{{field-label}}:</label>
    <div class="col-10">
        <textarea
            class="form-control"
            name="{{field-id}}"
            id="{{field-id}}"
            {{field-input-attributes}}
        ><?php echo ${{field-id}}; ?></textarea>
    </div>

</div>