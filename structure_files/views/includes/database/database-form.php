
<form id="{{database-lowercase-plural}}-form" action="/{{database-lowercase-plural}}edit" method="POST">

<div class="row">

    <div class="col-lg-12">

        {{database-form-fields}}

	    <div class="form-group row">

            <div class="col-2">&nbsp;</div>
            <div class="col-10">
               <a class="btn btn-primary" name="submit" onclick="javascript:$('#{{database-lowercase-plural}}-form').submit()" href="#"><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Save</a>
            </div>

        </div>

    </div>

</div>

</form>
