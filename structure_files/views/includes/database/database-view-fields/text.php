<div class="form-group row">

    <label for="{{field-id}}" class="col-2 col-form-label">{{field-label}}:</label>
    <div class="col-10">
        <?php if(
            isset(${{database-lowercase-singular}}->{{field-id}})
            && !empty(${{database-lowercase-singular}}->{{field-id}})
            &&!is_null(${{database-lowercase-singular}}->{{field-id}})
        ){ ?>
            <?php echo ${{database-lowercase-singular}}->{{field-id}}; ?>
        <?php } else if(
        isset(${{database-lowercase-singular}}->file_content->{{field-id}})
            && !empty(${{database-lowercase-singular}}->file_content->{{field-id}})
            &&!is_null(${{database-lowercase-singular}}->file_content->{{field-id}})
        ){ ?>
            <?php echo ${{database-lowercase-singular}}->file_content->{{field-id}}; ?>
        <?php } ?>
    </div>

</div>