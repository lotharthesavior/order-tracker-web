<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        {{table-column-labels}}
                        <th>Options</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty(${{database-lowercase-plural}}))
                    foreach (${{database-lowercase-plural}}->results as $key => $value) {

                        // TODO: temp solution
                        $value = (array) $value;
                        $value['file_content'] = (array) $value['file_content'];

                        ?>
                        
                        <tr class="table-active">
                            {{table-column-values}}
                            <td>
                                <a href="/{{database-lowercase-singular}}/<?php echo $value['id']; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <a
                                        onclick="if(!confirm('Do you really want to delete this record?')){return false}"
                                        href="/{{database-lowercase-singular}}-delete/<?php echo $value['id']; ?>"
                                ><i class="fa fa-trash" aria-hidden="true"></i></a>
                            </td>
                        </tr>

                    <?php } ?>

                </tbody>
            </table>
        </div>
    </div>
</div>
