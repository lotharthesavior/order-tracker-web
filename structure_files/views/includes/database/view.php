
<form id="{{database-lowercase-plural}}-form" action="/{{database-lowercase-plural}}edit" method="POST">

    <div class="row">

        <div class="col-lg-12">

            {{database-form-fields}}

        </div>

    </div>

</form>
