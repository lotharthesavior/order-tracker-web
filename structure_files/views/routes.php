<?php

// {{database-capitalize-plural}} ----------------------------------------------------------------

$app->get('/{{database-lowercase-plural}}', '{{database-capitalize-plural}}Controller:home');

$app->get('/{{database-lowercase-singular}}/{id}', '{{database-capitalize-plural}}Controller:view');

$app->get('/{{database-lowercase-singular}}-delete/{id}', '{{database-capitalize-plural}}Controller:remove');

$app->get('/{{database-lowercase-plural}}edit', '{{database-capitalize-plural}}Controller:create');

$app->get('/{{database-lowercase-plural}}edit/{id}', '{{database-capitalize-plural}}Controller:edit');

$app->post('/{{database-lowercase-plural}}edit', '{{database-capitalize-plural}}Controller:editPost');

// / {{database-capitalize-plural}} -------------------------------------------------------------