<?php

use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJarInterface;

use Models\Tracker;
use Models\TicketsComment;

class TicketsTest extends TestCase
{
    /**
     * 
     */
    protected $default_title = "Lorem Ipsum";

    /**
     * 
     */
    protected $model_config = ['username'=>'MT','userpassword'=>'MT'];

    public function setUp()
    {
        $config_json = file_get_contents("config.json");
        $config['settings'] = json_decode($config_json, true);
        
        $this->http = new \GuzzleHttp\Client(['base_uri' => 'http://' . $config['settings']['domain'] . '/']);
        $this->jar = new \GuzzleHttp\Cookie\CookieJar();
    }

    /**
     * Execute Authorization for requests that need those
     */
    private function executeAuth($extra_options = []){
        $default_options = [
            'form_params' => [
                'PATRON_ID' => 'savio',
                'PATRON_PID' => 'test'
            ],
            'cookies' => $this->jar
        ];

        $options = array_merge($default_options, $extra_options);

        $response = $this->http->request('POST', 'auth', $options);

        return $response;
    }

    /**
     * Create Ticket through request on minnet
     * 
     * @return $response
     */
    private function createTicket(){
        $response = $this->executeAuth();
        $post_data = [
            "sisn" => "",
            "status" => "Open",
            "title" => $this->default_title,
            "error_msg$1" => "",
            "type" => "Request",
            "m_notification" => "",
            "application" => "",
            "release" => "",
            "problem" => "<p>Lorem Ipsum Descriptus Some Any Language</p>"
        ];

        $options = [
            'form_params' => $post_data,
            'allow_redirects' => false,
            'cookies' => $this->jar
        ];

        // create ticket
        $response = $this->http->request('POST', 'ticketsedit', $options);

        return $response;
    }

    /**
     * Get Ticket
     */
    private function getTicket($params = ['TRACKER_NO' => '*']){
        $tickets = new Models\Tracker($this->model_config);

        return $tickets->search($params);
    }

    /**
     * Get Ticket Comment
     */
    private function getTicketsComment($params = ['TRACKER_NO' => '*']){
        $tickets_comment = new Models\TicketsComment($this->model_config);

        return $tickets_comment->search($params);
    }

    /**
     * Get Ticket Comment
     */
    private function getTicketsFix($params = ['FK_TICKET_FIX' => '*']){
        $tickets_comment = new Models\TrackerFix($this->model_config);

        return $tickets_comment->search($params);
    }

    /**
     * 
     */
    private function deleteTicket($param = []){
        if( empty($param) || !isset($param['sisn']) ){
            throw new Exception("Missing param", 1);
        }

        $tickets = new Models\Tracker($this->model_config);
        $tickets->sisn = $param['sisn'];
        $tickets->deleteRecord();
    }

    /**
     * 
     */
    private function deleteTicketComment($param = []){
        if( empty($param) || !isset($param['sisn']) ){
            throw new Exception("Missing param", 1);
        }

        $tickets_comment = new Models\TicketsComment($this->model_config);
        $tickets_comment->sisn = $param['sisn'];
        $tickets_comment->deleteRecord();
    }

    /**
     * 
     */
    private function deleteTicketFix($param = []){
        if( empty($param) || !isset($param['sisn']) ){
            throw new Exception("Missing param", 1);
        }

        $tracker_fix = new Models\TrackerFix($this->model_config);
        $tracker_fix->sisn = $param['sisn'];
        $tracker_fix->deleteRecord();
    }

    /**
     * It goes to login page
     */
    public function testRouteHome()
    {
        $response = $this->http->request('GET');
        $data = $response->getBody()->getContents();

        // Assert
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains("MIN.net", $data);
    }

    /**
     * Test Login Authorization
     */
    public function testAuth()
    {
        $response = $this->executeAuth();
        $data = $response->getBody()->getContents();

        $this->assertEquals(200, $response->getStatusCode());
        // $this->assertContains("MINISIS Inc.", $data);
    }

    /**
     * Test Login Authorization without redirection
     */
    public function testAuthWithoutRedirect(){
        $response = $this->executeAuth(['allow_redirects' => false]);

        // expect redirection if the login is ok 
        // (an error launch a 403 status code)
        $this->assertEquals(302, $response->getStatusCode());
    }

    /**
     * @internal some tests depends on this test to happen
     */
    public function testTicketCreation(){
        $response = $this->createTicket();
        $tickets_list = $this->getTicket(['TITLE' => $this->default_title]);

        // tests --
        $header_location = $response->getHeaderLine('Location');
        $result_status_code = $response->getStatusCode();
        
        $this->assertContains("/tickets", $header_location);
        $this->assertEquals(302, $result_status_code);
        $this->assertEquals(true, (count($tickets_list) > 0));
        // tests --

        // removing created ticket
        $this->deleteTicket(['sisn' => $tickets_list[0]->sisn[0]]);
    }

    /**
     * POST to /tickets
     * DATA: ['KEYWORD_CL' => 'text']
     */
    public function testTicketsSearchPage(){
        $response = $this->executeAuth();
        $response2 = $this->createTicket();

        // tests --
        $options = [
            'form_params' => [
                'KEYWORD_CL' => $this->default_title
            ],
            'cookies' => $this->jar
        ];
        $response3 = $this->http->request('POST', 'tickets', $options);
        $data = $response3->getBody()->getContents();
        $this->assertContains('Showing results for the search "Lorem Ipsum":', $data);
        // tests --

        // removing created ticket
        $tickets_list = $this->getTicket(['TITLE' => $this->default_title]);
        $this->deleteTicket(['sisn' => $tickets_list[0]->sisn[0]]);
    }
        
    /**
     * GET to /ticket/{sisn}
     * @internal require a test ticket created
     */
    public function testTicketPage(){
        $response = $this->executeAuth();
        $response2 = $this->createTicket();
        $tickets_list = $this->getTicket(['TITLE' => $this->default_title]);

        // tests --
        $sisn = $tickets_list[0]->sisn[0];
        $options = [
            'cookies' => $this->jar
        ];
        $response3 = $this->http->request('GET', ('ticket/'.$sisn), $options);
        $data = $response3->getBody()->getContents();
        $this->assertContains('View Ticket', $data);
        // tests --

        // removing created ticket
        $this->deleteTicket(['sisn' => $tickets_list[0]->sisn[0]]);
    }

    /**
     * POST to /tickets-comment-edit
     * DATA: ['KEYWORD_CL' => 'text']
     */
    public function testTicketsCommentCreation(){
        $response = $this->executeAuth();
        $response2 = $this->createTicket();
        $tickets_list = $this->getTicket(['TITLE' => $this->default_title]);

        // tests --
        $tracker_no = $tickets_list[0]->tracker_no[0];
        $options = [
            'form_params' => [
                'TRACKER_NO' => $tracker_no,
                'COMMENT_AUTHOR' => 'savio',
                'COMMENT_TEXT' => '<p>Lorem Ipsum Comment.</p>'
            ],
            'allow_redirects' => false,
            'cookies' => $this->jar
        ];
        $response3 = $this->http->request('POST', 'tickets-comment-edit', $options);
        $result_status_code = $response3->getStatusCode();
        $ticket_comment = $this->getTicketsComment(['COMMENT_TEXT' => '<p>Lorem Ipsum Comment.</p>']);
        $this->assertEquals(302, $result_status_code);
        $this->assertEquals(true, (count($ticket_comment) > 0));
        // tests --

        // removing created ticket
        $this->deleteTicket(['sisn' => $tickets_list[0]->sisn[0]]);
        $this->deleteTicketComment(['sisn' => $ticket_comment[0]->sisn[0]]);
    }

    /**
     * POST to /tracker-fix-edit
     * DATA: ['KEYWORD_CL' => 'text']
     */
    public function testTicketsFixCreation(){
        $response = $this->executeAuth();
        $response2 = $this->createTicket();
        $tickets_list = $this->getTicket(['TITLE' => $this->default_title]);

        // tests --
        $tracker_no = $tickets_list[0]->tracker_no[0];
        $current_date = new \DateTime("now");
        $options = [
            'form_params' => [
                'FK_TICKET_FIX' => $tracker_no,
                'FIXED_VERNO' => '9.05.00 2014-03-05',
                'SCOPE' => 'INTERNAL',
                'FIXED_BY' => 'savio',
                'FIX_DATE' => $current_date->format('Y-m-d H:i:s'),
                'FIX' => '<p>Lorem Ipsum Fix.</p>'
            ],
            'allow_redirects' => false,
            'cookies' => $this->jar
        ];
        $response3 = $this->http->request('POST', 'tracker-fix-edit', $options);
        $result_status_code = $response3->getStatusCode();
        $ticket_fix = $this->getTicketsFix(['FIX' => '<p>Lorem Ipsum Fix.</p>']);
        $this->assertEquals(302, $result_status_code);
        $this->assertEquals(true, (count($ticket_fix) > 0));
        // tests --

        // removing created ticket
        $this->deleteTicket(['sisn' => $tickets_list[0]->sisn[0]]);
        $this->deleteTicketFix(['sisn' => $ticket_fix[0]->sisn[0]]);
    }

    /**
     * GET to /ticketsedit/{sisn}
     * @internal require a test ticket created
     */
    public function testTicketEditionPage(){
        $response = $this->executeAuth();
        $response2 = $this->createTicket();
        $tickets_list = $this->getTicket(['TITLE' => $this->default_title]);

        // tests --
        $sisn = $tickets_list[0]->sisn[0];
        $options = [
            'cookies' => $this->jar
        ];
        $response3 = $this->http->request('GET', 'ticketsedit/' . $sisn, $options);
        $data = $response3->getBody()->getContents();
        $result_status_code = $response3->getStatusCode();
        $this->assertEquals(200, $result_status_code);
        $this->assertContains('Edit Ticket', $data);
        // tests --

        // removing created ticket
        $this->deleteTicket(['sisn' => $tickets_list[0]->sisn[0]]);
    }

    /**
     * GET to /ticketsedit
     * @internal require a test ticket created
     */
    public function testTicketEditionPost(){
        $response = $this->executeAuth();
        $response2 = $this->createTicket();
        $tickets_list = $this->getTicket(['TITLE' => $this->default_title]);

        // tests --
        $sisn = $tickets_list[0]->sisn[0];
        $options = [
            'form_params' => [
                "sisn" => $sisn,
                "status" => "Open",
                "title" => $this->default_title,
                "error_msg$1" => "test",
                "error_msg$2" => "test2",
                "type" => "Information",
                "problem" => "<p>Lorem Ipsum Problem. Change 1.</p>",
                "reported_by" => "savio",
                "reported_on" => "2017-01-30 13:14:59",
            ],
            'allow_redirects' => false,
            'cookies' => $this->jar
        ];
        $response3 = $this->http->request('POST', 'ticketsedit', $options);
        $result_status_code = $response3->getStatusCode();
        $header_location = $response3->getHeaderLine('Location');
        $tickets_list = $this->getTicket(['TITLE' => $this->default_title]);

        $this->assertContains("/tickets", $header_location);
        $this->assertEquals(302, $result_status_code);
        $this->assertContains("<p>Lorem Ipsum Problem. Change 1.</p>", $tickets_list[0]->problem[0]);
        // tests --

        // removing created ticket
        $this->deleteTicket(['sisn' => $tickets_list[0]->sisn[0]]);
    }

    /**
     * GET to /tickets-responsible-edit
     * @internal require a test ticket created
     */
    public function testTicketResponsiblePost(){
        $response = $this->executeAuth();
        $response2 = $this->createTicket();
        $tickets_list = $this->getTicket(['TITLE' => $this->default_title]);

        // tests --
        $tracker_no = $tickets_list[0]->tracker_no[0];
        $options = [
            'form_params' => [
                "tracker_no" => $tracker_no,
                "FK_RESPONSIBLE" => "savio"
            ],
            'allow_redirects' => false,
            'cookies' => $this->jar
        ];
        $response3 = $this->http->request('POST', 'tickets-responsible-edit', $options);
        $result_status_code = $response3->getStatusCode();
        $header_location = $response3->getHeaderLine('Location');
        $tickets_list = $this->getTicket(['TITLE' => $this->default_title]);

        $this->assertContains("/tickets", $header_location);
        $this->assertEquals(302, $result_status_code);
        $this->assertContains("savio", $tickets_list[0]->responsible_grp[0]['fk_responsible'][0]);
        // tests --

        // removing created ticket
        $this->deleteTicket(['sisn' => $tickets_list[0]->sisn[0]]);
    }

    /**
     * GET to /ticket/{sisn}
     * @internal require a test ticket created
     */
    public function testDocumentationPage(){
        $response = $this->executeAuth();

        $response = $this->http->request('GET', '/documentation', [
            'cookies' => $this->jar
        ]);

        $data = $response->getBody()->getContents();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains("Documentation", $data);
    }

    /**
     * 
     */
    public function testReleasePage(){
        $response = $this->executeAuth();

        $response = $this->http->request('GET', '/software-updates', [
            'cookies' => $this->jar
        ]);

        $data = $response->getBody()->getContents();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains("Documentation", $data);
    }

    // release page
    // send mail from release page
    // news and events page
    // our history page
}