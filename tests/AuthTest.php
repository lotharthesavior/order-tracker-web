<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJarInterface;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;

/**
 *
 */
final class AuthTest extends TestCase
{

    public function setUp(){
    
        $this->jar = new \GuzzleHttp\Cookie\CookieJar();

    }

    /**
     * @beforeClass
     */
    public function setUpSession(){
    
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

    }

    /**
     * @before
     */
    public function setupConfig(){

        $config_json = file_get_contents("config.json");
        $this->config = json_decode($config_json, true);
        $messages_json = file_get_contents("messages.json");
        $this->messages = json_decode($messages_json, true);

        $this->logger = new Logger('tests_log');
        $this->logger->pushHandler(new StreamHandler(__DIR__ . '/phpunit_tests.log', Logger::DEBUG));
        $this->logger->pushHandler(new FirePHPHandler());
        $this->client = new Client([
            'base_uri' => $this->config['protocol'] . "://" . $this->config['domain'],
            'verify' => false
        ]);

    }

    /**
     * @afterClass
     */
    public function removeUsers(): void{

        $user_model = new \Models\Users;
        $user_model->remove(['email' => 'lotharpoeta@gmail.com']);

    }

    /**
     *
     */
    public function testRegisterFormMissingFields(): void{

        $response = $this->client->request('POST', '/register', [
            'form_params' => [
                'email' => '',
                'password' => '',
                'test' => 1
            ],
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'allow_redirects' => false
        ]);

        $this->logger->info('testRegisterFormMissingFields: ' . $response->getBody()->getContents());

        $this->assertEquals(
            200,
            $response->getStatusCode()
        );

        $this->assertEquals(
            $this->messages['registration_missing_fields'],
            $response->getBody()->getContents()
        );

    }

    /**
     *
     */
    public function testRegisterForm(): void{

        $response = $this->client->request('POST', '/register', [
            'form_params' => [
                'email' => 'lotharpoeta@gmail.com', 
                'password' => 'test',
                'test' => 1
            ],
            'allow_redirects' => false
            ,'cookies' => $this->jar
        ]); 

        $this->assertEquals(
            200,
            $response->getStatusCode()
        );  

        $this->assertEquals(
            $this->messages['register_success'],
            $response->getBody()->getContents()
        );

    }

    /**
     *
     */
    public function testLoginInvalidCredentials(): void{

        $utility_helper = new \Helpers\UtilityHelper;

        $response = $this->client->request('POST', '/login', [
            'form_params' => [
                'email' => '',
                'password' => ''
            ]
            , 'cookies' => $this->jar
        ]);

        $this->assertEquals(
            301,
            $response->getStatusCode()
        );

        $this->assertEquals(
            $this->messages['invalid_credentials'],
            $utility_helper->getLastMessage()
        );

    }

    /**
     *
     */
    public function testLogin(): void{

        $utility_helper = new \Helpers\UtilityHelper;

        $response = $this->client->request('POST', '/login', [
            'form_params' => [
                'email' => 'lotharpoeta@gmail.com', 
                'password' => 'test'
            ]
            , 'cookies' => $this->jar
        ]); 

        $this->assertEquals(
            301,
            $response->getStatusCode()
        );  

        $this->assertEquals(
            $this->messages['login_success'],
            $utility_helper->getLastMessage()
        );

    }

    /**
     *
     */
    public function testForgetPassword(): void{

        $utility_helper = new \Helpers\UtilityHelper;        

        $response = $this->client->request('POST', '/forget-password', [
            'form_params' => [
                'email' => 'lotharpoeta@gmail.com'
            ]
            , 'cookies' => $this->jar 
        ]); 

        $this->assertEquals(
            301,
            $response->getStatusCode()
        );  

        $this->assertEquals(
            $this->messages['forget_password_email_sent'],
            $utility_helper->getLastMessage()
        );

    }

    /**
     *
     */
    public function testForgetPasswordWithNonRegisteredEmail(): void{
 
        $utility_helper = new \Helpers\UtilityHelper;
   
        $response = $this->client->request('POST', '/forget-password', [
            'form_params' => [
                'email' => 'lotharpoetass@gmail.com'
            ]
            , 'cookies' => $this->jar
        ]);

        $this->assertEquals(
            301,
            $response->getStatusCode()
        );

        $this->assertEquals(
            $this->messages['forget_password_email_not_registered'],
            $utility_helper->getLastMessage()
        );
    
    }

    /**
     *
     */
    public function testNewPasswordMismatch(): void{

        $utility_helper = new \Helpers\UtilityHelper;
        $utility_helper->create_new_password_solicitation("lotharpoeta@gmail.com");
        $token = $utility_helper->getLastPasswordSolicitationToken("lotharpoeta@gmail.com");

        $response = $this->client->request('POST', '/new-password', [
            'form_params' => [
                'email' => 'lotharpoetass@gmail.com',
                'token' => $token,
                'password' => 'test2',
                'password_confirmation' => 'test'
            ]
             , 'cookies' => $this->jar 
        ]);

        $this->assertEquals(
            301,
            $response->getStatusCode()
        );

        $this->assertEquals(
            $this->messages['new_password_mismatch'],
            $utility_helper->getLastMessage()
        );

    }

    /** 
     *
     */
    public function testNewPassword(): void{

        $utility_helper = new \Helpers\UtilityHelper;
        $utility_helper->create_new_password_solicitation("lotharpoeta@gmail.com");
        $token = $utility_helper->getLastPasswordSolicitationToken("lotharpoeta@gmail.com");

        $response = $this->client->request('POST', '/new-password', [
            'form_params' => [
                'email' => 'lotharpoetass@gmail.com',
                'token' => $token,
                'password' => 'test2',
                'password_confirmation' => 'test2'
            ]
            , 'cookies' => $this->jar
        ]); 

        $this->assertEquals(
            301,
            $response->getStatusCode()
        );  

        $this->assertEquals(
            $this->messages['new_password_success'],
            $utility_helper->getLastMessage()
        );

    }   

}
