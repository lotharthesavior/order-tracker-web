<?php

use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJarInterface;

class DocumentationTest extends TestCase
{
    /**
     * 
     */
    protected $model_config = ['username'=>'MT','userpassword'=>'MT'];

    public function setUp()
    {
        $this->http = new \GuzzleHttp\Client(['base_uri' => 'http://tracker.app/']);
        $this->jar = new \GuzzleHttp\Cookie\CookieJar();
    }

    /**
     * Execute Authorization for requests that need those
     */
    private function executeAuth($extra_options = []){
        $default_options = [
            'form_params' => [
                'PATRON_ID' => 'savio',
                'PATRON_PID' => 'test'
            ],
            'cookies' => $this->jar
        ];

        $options = array_merge($default_options, $extra_options);

        $response = $this->http->request('POST', 'auth', $options);

        return $response;
    }

    /**
     * GET to /ticket/{sisn}
     * @internal require a test ticket created
     */
    public function testDocumentationPage(){
        $response = $this->executeAuth();

        $response = $this->http->request('GET', '/documentation', [
            'cookies' => $this->jar
        ]);

        $data = $response->getBody()->getContents();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains("Documentation", $data);
    }
}