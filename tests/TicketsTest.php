<?php

use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJarInterface;

// Obs.: this can help to debug
use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local;
// $adapter = new Local(__DIR__);
// $filesystem = new Filesystem($adapter);
// $filesystem->write("post_output.html", $response3->getBody()->getContents());
// exit('test');

require __DIR__ . "/../vendor/autoload.php";

class TicketsTest extends TestCase
{
    /**
     * 
     */
    protected $default_title = "Lorem Ipsum";

    /**
     * 
     */
    protected $model_config = ['username'=>'MT','userpassword'=>'MT'];

    public function setUp()
    {
        $config_json = file_get_contents("config.json");
        $config['settings'] = json_decode($config_json, true);
        
        $this->http = new \GuzzleHttp\Client(['base_uri' => 'http://' . $config['settings']['domain'] . '/']);
        $this->jar = new \GuzzleHttp\Cookie\CookieJar();
    }

    /**
     * Execute Authorization for requests that need those
     */
    private function _executeAuth($extra_options = []){
        $default_options = [
            'form_params' => [
                'email' => 'savio@savioresende.com.br',
                'password' => 'lothar5'
            ],
            'cookies' => $this->jar
        ];

        $options = array_merge($default_options, $extra_options);

        $response = $this->http->request('POST', '/login', $options);

        return $response;
    }

    /**
     * Create Ticket through request on minnet
     * 
     * @return void
     */
    private function _createTicket(){
        $ticket_model = new \Models\Tickets();

        $ticket_data = [
            "id" => "",
            "title" => $this->default_title,
            "description" => "Mussum Ipsum, cacilds vidis litro abertis. Si num tem leite então bota uma pinga aí cumpadi! Manduma pindureta quium dia nois paga. Paisis, filhis, espiritis santis. Aenean aliquam molestie leo, vitae iaculis nisl. ",
            "status" => "Open",
            "type" => "Order",
        ];

        $ticket_model->save($ticket_data);
    }

    /**
     * @param Int $id
     * @return void
     */
    private function _deleteTicket($id){
        $ticket_model = new \Models\Tickets();

        $ticket_model->deleteRecord($id);
    }

    /**
     * 
     */
    private function _findOneTestTicket(){
        $ticket_model = new \Models\Tickets();
        
        $results = $ticket_model->search([
            'title' => "Lorem Ipsum"
        ]);

        return $results;
    }

    /**
     * @afterClass
     */
    public static function deleteAllTestTickets()
    {
        $ticket_model = new \Models\Tickets();
        
        $results = $ticket_model->search([
            'title' => "Lorem Ipsum"
        ]);

        foreach ($results as $key => $record) {
            $ticket_model->deleteRecord($record->id);
        }
    }

    /**
     * 
     */
    public function testDeleteTicket(){
        $this->_executeAuth();

        $this->_createTicket();

        $ticket_model = new \Models\Tickets();
        
        $result = $ticket_model->search([
            'title' => $this->default_title
        ]);

        $id = $result[0]->id;
        
        $options = [
            'allow_redirects' => false,
            'cookies' => $this->jar
        ];

        $response = $this->http->request('GET', '/ticket-delete/' . $id, $options);

        $this->assertEquals("302", $response->getStatusCode());

        $result = $ticket_model->find($id);

        $this->assertTrue( empty($result) );
    }

    /**
     * 
     */
    public function testTicketCreation(){
        $this->_executeAuth();

        $post_data = [
            "id" => "",
            "title" => $this->default_title,
            "description" => "Mussum Ipsum, cacilds vidis litro abertis. Si num tem leite então bota uma pinga aí cumpadi! Manduma pindureta quium dia nois paga. Paisis, filhis, espiritis santis. Aenean aliquam molestie leo, vitae iaculis nisl. ",
            "status" => "Open",
            "type" => "Order",
        ];

        $options = [
            'form_params' => $post_data,
            'allow_redirects' => false,
            'cookies' => $this->jar
        ];

        // create ticket
        $response = $this->http->request('POST', '/ticketsedit', $options);

        $header_location = $response->getHeaderLine('Location');

        // TODO: test if this item is in the database
            // test on git
            // test searching
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertContains("/tickets", $header_location);
    }

    /**
     * 
     */
    public function testTicketSearchOneParam(){
        $this->_executeAuth();

        self::deleteAllTestTickets();

        $this->_createTicket();

        $ticket = $this->_findOneTestTicket();

        $options = [
            'allow_redirects' => false,
            'cookies' => $this->jar
        ];

        // create ticket
        $response = $this->http->request('GET', '/tickets/title/' . $this->default_title, $options);

        $this->assertEquals("200", $response->getStatusCode());

        // contains the correct title
        $this->assertContains("Tickets results for " . $this->default_title, $response->getBody()->getContents());

        // contains the deletion link
        $this->assertContains("ticket-delete/ " . $ticket[0]->id, $response->getBody()->getContents());

        // contains the edition link
        $this->assertContains("ticketsedit/ " . $ticket[0]->id, $response->getBody()->getContents());
    }

    /**
     * 
     */
    public function testTicketEdition(){
        $this->_executeAuth();

        $this->_createTicket();

        $ticket = $this->_findOneTestTicket();

        $options = [
            'allow_redirects' => false,
            'cookies' => $this->jar
        ];

        // create ticket
        $response = $this->http->request('GET', '/ticketsedit/' . $ticket[0]->id, $options);

        $this->assertEquals("200", $response->getStatusCode());

        // contains the correct title
        $this->assertContains("Ticket Edit: " . $this->default_title, $response->getBody()->getContents());
    }

}