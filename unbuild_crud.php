<?php
/**
 *
 */

require __DIR__ . '/vendor/autoload.php';

use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local;


$adapter = new Local(__DIR__.'/');
$filesystem = new Filesystem($adapter);


// ##################################
// Inputs
// ##################################
// databases names --
// Arg1: {{database-capitalize-plural}}
//$database_capitalize_plural = 'Notes';
if (!isset($_POST['database_capitalize_plural'])) {
    echo "<div><span style='color:red;'>Missing Field:</span> database_capitalize_plural.</div>";exit;
}
$database_capitalize_plural = $_POST['database_capitalize_plural'];

// Arg2: {{database-capitalize-singular}}
//$database_capitalize_singular = 'Note';
if (!isset($_POST['database_capitalize_singular'])) {
    echo "<div><span style='color:red;'>Missing Field:</span> database_capitalize_singular.</div>";exit;
}
$database_capitalize_singular = $_POST['database_capitalize_singular'];

// Arg3: {{database-lowercase-plural}}
//$database_lowercase_plural = 'notes';
if (!isset($_POST['database_lowercase_plural'])) {
    echo "<div><span style='color:red;'>Missing Field:</span> database_lowercase_plural.</div>";exit;
}
$database_lowercase_plural = $_POST['database_lowercase_plural'];

// Arg4: {{database-lowercase-singular}}
//$database_lowercase_singular = 'note';
if (!isset($_POST['database_lowercase_singular'])) {
    echo "<div><span style='color:red;'>Missing Field:</span> database_lowercase_singular.</div>";exit;
}
$database_lowercase_singular = $_POST['database_lowercase_singular'];
// --

// Arg5: database fields - {{database-fields}}, {{protected-model-database-fields}} --
//$database_fields = [
//    'title' => 'string',
//    'content' => 'string'
//];
if (!isset($_POST['database_fields'])) {
    echo "<div><span style='color:red;'>Missing Field:</span> database_fields.</div>";exit;
}
$database_fields = $_POST['database_fields'];
// --

// Arg6: database required fields - {{database-required-fields}} --
// TODO
if (!isset($_POST['database_required_fields'])) {
    echo "<div><span style='color:red;'>Missing Field:</span> database_required_fields.</div>";exit;
}
$database_required_fields = $_POST['database_required_fields'];

// Arg7: template dir name --
//$template_dir_name = 'sb-admin';
if (!isset($_POST['template_dir_name'])) {
    echo "<div><span style='color:red;'>Missing Field:</span> template_dir_name.</div>";exit;
}
$template_dir_name = $_POST['template_dir_name'];
// --


// ##################################
// Delete Views
// ##################################
// database.php --
    $step_front_1_result = '';

    // $filesystem
    $template_database_address = 'public/' . $template_dir_name . '/' . $database_lowercase_plural . '.php';

    $step_front_1_result = '<p><span style="color:red">Warning:</span> public/template-dir/database.php not deleted!</p>';
    if ($filesystem->has($template_database_address)) {
        $filesystem->delete($template_database_address);
        $step_front_1_result = '<p><span style="color:green">Success:</span> public/template-dir/database.php deleted!</p>';
    }
    // --

    echo $step_front_1_result;
// --

// database-edit.php --
    $step_front_2_result = '';

    // $filesystem
    $template_database_edit_address = 'public/' . $template_dir_name . '/' . $database_lowercase_plural . '-edit.php';

    $step_front_2_result = '<p><span style="color:red">Warning:</span> public/template-dir/database-edit.php not deleted!</p>';
    if ($filesystem->has($template_database_edit_address)) {
        $filesystem->delete($template_database_edit_address);
        $step_front_2_result = '<p><span style="color:green">Success:</span> public/template-dir/database-edit.php deleted!</p>';
    }
    // --

    echo $step_front_2_result;
// --

// database-view.php --
    $step_front_3_result = '';

    // $filesystem
    $template_database_view_address = 'public/' . $template_dir_name . '/' . $database_lowercase_plural . '-view.php';

    $step_front_3_result = '<p><span style="color:red">Warning:</span> public/template-dir/database-view.php not deleted!</p>';
    if ($filesystem->has($template_database_view_address)) {
        $filesystem->delete($template_database_view_address);
        $step_front_3_result = '<p><span style="color:green">Success:</span> public/template-dir/database-view.php created!</p>';
    }
    // --

    echo $step_front_3_result;
// --

// includes/users/database-form.php --
    $step_4_result = '';

    // $filesystem
    $template_database_include_form_address = 'public/' . $template_dir_name . '/includes/' . $database_lowercase_plural . '/' . $database_lowercase_plural . '-form.php';

    $step_4_result = '<p><span style="color:red">Warning:</span> public/template-dir/includes/database/database-form.php not deleted!</p>';
    if ($filesystem->has($template_database_include_form_address)) {
        $filesystem->delete($template_database_include_form_address);
        $step_4_result = '<p><span style="color:green">Success:</span> public/template-dir/includes/database/database-form.php deleted!</p>';
    }

    echo $step_4_result;
    // --
// --

// includes/users/table.php --
    $step_5_result = '';

    // $filesystem
    $template_database_include_table_address = 'public/' . $template_dir_name . '/includes/' . $database_lowercase_plural . '/table.php';

    $step_5_result = '<p><span style="color:red">Warning:</span> public/template-dir/includes/database/table.php not deleted!</p>';
    if ($filesystem->has($template_database_include_table_address)) {
        $filesystem->delete($template_database_include_table_address);
        $step_5_result = '<p><span style="color:green">Success:</span> public/template-dir/includes/database/table.php deleted!</p>';
    }

    echo $step_5_result;
    // --
// --

// includes/users/view.php
    $step_6_result = '';

    // $filesystem
    $template_database_include_view_address = 'public/' . $template_dir_name . '/includes/' . $database_lowercase_plural . '/view.php';

    $step_6_result = '<p><span style="color:red">Warning:</span> public/template-dir/includes/database/view.php not deleted!</p>';
    if ($filesystem->has($template_database_include_view_address)) {
        $filesystem->delete($template_database_include_view_address);
        $step_6_result = '<p><span style="color:green">Success:</span> public/template-dir/includes/database/view.php deleted!</p>';
    }

    echo $step_6_result;
    // --
// --
// ##################################


// ##################################
// Create Models
// ##################################
// Database.php
    $step_1_result = '';

    // $filesystem
    $database_model_address = 'Models/Generated/' . $database_capitalize_plural . '.php';

    $step_1_result = '<p><span style="color:red">Warning:</span> (Model) Database.php not deleted!</p>';
    if ($filesystem->has($database_model_address)) {
        $filesystem->delete($database_model_address);
        $step_1_result = '<p><span style="color:green">Success:</span> (Model) Database.php deleted!</p>';
    }
    // --

    echo $step_1_result;
// --

// DB/Database-singular.php --
    $step_2_result = '';

    // $filesystem
    $database_singular_model_address = 'Models/DB/Generated/' . $database_capitalize_singular . '.php';

    $step_2_result = '<p><span style="color:red">Warning:</span> (Model) DB/Database-singular.php not deleted!</p>';
    if ($filesystem->has($database_singular_model_address)) {
        $filesystem->delete($database_singular_model_address);
        $step_2_result = '<p><span style="color:green">Success:</span> (Model) Database-singular.php deleted!</p>';
    }
    // --

    echo $step_2_result;
// --
// ##################################


// ##################################
// Create Controller
// ##################################
// DatabaseController.php
    $step_3_result = '';

    // $filesystem
    $database_controller_address = 'Controllers/Generated/' . $database_capitalize_plural . 'Controller.php';

    $step_3_result = '<p><span style="color:red">Warning:</span> (Controller) DatabaseController.php not deleted!</p>';
    if ($filesystem->has($database_controller_address)) {
        $filesystem->delete($database_controller_address);
        $step_3_result = '<p><span style="color:green">Success:</span> (Controller) DatabaseController.php deleted!</p>';
    }
    // --

    echo $step_3_result;
// ##################################


// ##################################
// Create Routes
// ##################################
// create routes --
    $step_4_result = '';

    // create route file
    $database_routes_address = 'routes/' . $database_lowercase_plural . '-routes.php';
    // $routes_file
    $step_4_result = '<p><span style="color:red">Warning:</span> database-routes.php not deleted!</p>';
    if ($filesystem->has($database_routes_address)) {
        $filesystem->delete($database_routes_address);
        $step_4_result = '<p><span style="color:green">Success:</span> database-routes.php deleted!</p>';
    }
    // --

    echo $step_4_result;
// --
// ##################################


// ##################################
// Create Navigation
// ##################################
// create navigation --
    $step_5_result = '';

    // create route file
    $database_navigation_address = 'public/' . $template_dir_name . '/includes/navigations/' . $database_lowercase_plural . '-navigation.php';
    // $navigation_file
    $step_5_result = '<p><span style="color:red">Warning:</span> database-navigation.php not deleted!</p>';
    if ($filesystem->has($database_navigation_address)) {
        $filesystem->delete($database_navigation_address);
        $step_5_result = '<p><span style="color:green">Success:</span> database-navigation.php deleted!</p>';
    }
    // --

    echo $step_5_result;
// --
// ##################################


// ##################################
// Create ACL Routes
// ##################################
// create acl routes --
    $step_6_result = '';

    // create route file
    $database_acl_routes_address = 'routes/' . $database_lowercase_plural . '-acl-routes.php';
    // $acl_routes_file
    $step_6_result = '<p><span style="color:red">Warning:</span> database-acl-routes.php not deleted!</p>';
    if ($filesystem->has($database_acl_routes_address)) {
        $filesystem->delete($database_acl_routes_address);
        $step_6_result = '<p><span style="color:green">Success:</span> database-acl-routes.php deleted!</p>';
    }
    // --

    echo $step_6_result;
// --
// ##################################

// ##################################
// Create Containers
// ##################################
// create controllers container --
    $step_7_result = '';

    // create route file
    $database_controller_container_address = 'Controllers/Generated/' . $database_lowercase_plural . '-container.php';
    // $controller_container_file
    $step_7_result = '<p><span style="color:red">Warning:</span> DatabaseController-container.php not deleted!</p>';
    if ($filesystem->has($database_controller_container_address)) {
        $filesystem->delete($database_controller_container_address);
        $step_7_result = '<p><span style="color:green">Success:</span> DatabaseController-container.php deleted!</p>';
    }
    // --

    echo $step_7_result;
// --

// create model container --
    $step_8_result = '';

    // create route file
    $database_model_container_address = 'Models/Generated/' . $database_lowercase_plural . '-container.php';
    // $database_model_container_file
    $step_8_result = '<p><span style="color:red">Warning:</span> Database-container.php not deleted!</p>';
    if ($filesystem->has($database_model_container_address)) {
        $filesystem->delete($database_model_container_address);
        $step_8_result = '<p><span style="color:green">Success:</span> Database-container.php deleted!</p>';
    }
    // --

    echo $step_8_result;
// --

// create model singular container --
    $step_9_result = '';

    // create route file
    $database_model_singular_container_address = 'Models/DB/Generated/' . $database_lowercase_singular . '-container.php';
    // database_model_singular_container_file
    $step_9_result = '<p><span style="color:red">Warning:</span> Database-singular-container.php not deleted!</p>';
    if ($filesystem->has($database_model_singular_container_address)) {
        $filesystem->delete($database_model_singular_container_address);
        $step_9_result = '<p><span style="color:green">Success:</span> Database-singular-container.php deleted!</p>';
    }
    // --

    echo $step_9_result;
// --
// ##################################